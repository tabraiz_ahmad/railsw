package com.blueshield.appsws.preferences;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceManager {

    private static final String PREF_NAME = "§PREF";
    private static final String KEY_TOKEN = "RAILSWS.TOKEN";
    private static final String KEY_FCM_TOKEN = "RAILSWS.FCM";
    private static final String KEY_ID = "RAILSWS.ID";
    private static final String KEY_MESSAGE_COUNT = "RAILSWS.MESSAGE_COUNT";


    private static final String KEY_FIRST_NAME = "RAILSWS.FIRST_NAME";
    private static final String KEY_LAST_NAME = "RAILSWS.LAST_NAME";
    private static final String KEY_PROFILE_EMAIL = "RAILSWS.PROFILE_EMAIL";

    private static PreferenceManager sInstance;
    private final SharedPreferences mPref;

    private PreferenceManager(Context context) {
        mPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);

    }

    public static synchronized void initializeInstance(Context context) {
        if (sInstance == null) {
            sInstance = new PreferenceManager(context);
        }
    }

    public static synchronized PreferenceManager getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException(PreferenceManager.class.getSimpleName() +
                    " is not initialized, call initializeInstance(.) method first.");
        }
        return sInstance;
    }

    public void setToken(String value) {
        mPref.edit()
                .putString(KEY_TOKEN, value)
                .apply();
    }

    public void setFCMToken(String value) {
        mPref.edit()
                .putString(KEY_FCM_TOKEN, value)
                .apply();
    }


    public void setId(String value) {
        mPref.edit()
                .putString(KEY_ID, value)
                .apply();
    }


    public String getToken() {
        return mPref.getString(KEY_TOKEN, null);
    }

    public String getFCMToken() {
        return mPref.getString(KEY_FCM_TOKEN, null);
    }

    public String getId() {
        return mPref.getString(KEY_ID, null);
    }


    public String getFirstName() {
        return mPref.getString(KEY_FIRST_NAME, null);
    }

    public String getLastName() {
        return mPref.getString(KEY_LAST_NAME, null);
    }


    public void setFirstName(String value) {
        mPref.edit()
                .putString(KEY_FIRST_NAME, value)
                .apply();
    }

    public void setLastName(String value) {
        mPref.edit()
                .putString(KEY_LAST_NAME, value)
                .apply();
    }

    public String getProfileEmail() {
        return mPref.getString(KEY_PROFILE_EMAIL, null);
    }

    public void setProfileEmail(String value) {
        mPref.edit()
                .putString(KEY_PROFILE_EMAIL, value)
                .apply();
    }


    // called in LoginFragment
    public void saveProfile(String firstName, String lastName, String email, String token, String id) {
        setFirstName(firstName);
        setLastName(lastName);
        setProfileEmail(email);
        setToken(token);
        setId(id);
    }

    public void remove(String key) {
        mPref.edit()
                .remove(key)
                .apply();
    }

    public void clear() {
        remove(KEY_FIRST_NAME);
        remove(KEY_LAST_NAME);
        remove(KEY_TOKEN);
        remove(KEY_ID);
        remove(KEY_PROFILE_EMAIL);

    }

    public Boolean tokenIsEmpty() {
        if (getToken() != null)
            return getToken().isEmpty();
        else
            return true;
    }
}
