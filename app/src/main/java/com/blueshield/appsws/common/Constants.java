package com.blueshield.appsws.common;

public class Constants {
//    public static String BASE_URL = "http://wms.railsws.com.au/index.php/";
    public static String PROFIEL_IMAGE_BASE_URL = "http://wms.railsws.com.au/files/profile_images/";
    public static String GOOGLE_DRIVE_URL = "https://docs.google.com/gview?embedded=true&url=";
//    public static String FILES_URL = GOOGLE_DRIVE_URL + BASE_URL + "projects/download_file/";

    public static String SUCCESS = "success";
    public static final int TIMEOUT = 30;
    public static final String JOB_ID = "Job_id";
    public static final String JOB_ONLY_ID = "id";
    public static final String MESSAGE_ID = "MESSAGE_id";
    public static final String USER_NAME = "USER_NAME";
    public static final String USER_NAME_ARRAY = "USER_NAME_ARRAY";
    public static final String USER_ID = "MESSAGE_id";
    public static final String IS_ACCEPTABLE = "IS_ACCEPTABLE";
    public static final String RESOURCE_ID = "RESOURCE_ID";
    public static final int ADD_AVAILIBILTY_REQUEST_CODE = 999;
    public static final int NOTIFICATION_ID = 786;
    public static final String NOTIFICATION_TAG = "RailSWSNotification";

}
