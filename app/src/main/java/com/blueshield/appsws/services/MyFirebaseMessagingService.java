package com.blueshield.appsws.services;

import android.util.Log;

import com.blueshield.appsws.otto.BusProvider;
import com.blueshield.appsws.otto.JobChangedEvent;
import com.blueshield.appsws.otto.MessageEvent;
import com.blueshield.appsws.preferences.PreferenceManager;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    public static final String channelId = "9999";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    @Override
    public void onNewToken(String token) {
        Log.d("TAG", "Refreshed token: " + token);
        PreferenceManager.getInstance().setFCMToken(token);
        /*Need to update on server here*/
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Handle data payload of FCM messages.
        handleNotification(remoteMessage);

    }

    private void handleNotification(RemoteMessage remoteMessage) {


        if (remoteMessage.getData().keySet().contains("message_info")) {
            try {

                JSONObject jsonObject = new JSONObject(remoteMessage.getData().get("message_info"));
                if (jsonObject.has("type") && jsonObject.get("type").equals("job")) {

                    int jobId = Integer.parseInt(String.valueOf(jsonObject.get("project_id")));
                    int resourceId = Integer.parseInt(String.valueOf(jsonObject.get("resource_requirement_id")));
                    BusProvider.getInstance().post(new JobChangedEvent(jobId, resourceId));
                } else {
                    String messageId = jsonObject.get("main_message_id").toString();
                    BusProvider.getInstance().post(new MessageEvent(messageId));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        BusProvider.getInstance().post(remoteMessage);


    }

}
