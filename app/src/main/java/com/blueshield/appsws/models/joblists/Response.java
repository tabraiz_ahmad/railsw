
package com.blueshield.appsws.models.joblists;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("new_jobs_approval")
    @Expose
    private List<NewJobsApproval> newJobsApproval = null;
    @SerializedName("all_jobs")
    @Expose
    private List<Job> allJobs = null;

    public List<NewJobsApproval> getNewJobsApproval() {
        return newJobsApproval;
    }

    public void setNewJobsApproval(List<NewJobsApproval> newJobsApproval) {
        this.newJobsApproval = newJobsApproval;
    }

    public List<Job> getAllJobs() {
        return allJobs;
    }

    public void setAllJobs(List<Job> allJobs) {
        this.allJobs = allJobs;
    }

}
