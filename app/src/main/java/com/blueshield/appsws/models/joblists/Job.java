
package com.blueshield.appsws.models.joblists;

import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Job implements Comparable<Job>{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("job_id")
    @Expose
    private Integer jobId;
    @SerializedName("client_id")
    @Expose
    private Integer clientId;
    @SerializedName("project_name")
    @Expose
    private String projectName;
    @SerializedName("booking_id")
    @Expose
    private Integer bookingId;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("shift_title")
    @Expose
    private String shiftTitle;
    @SerializedName("worksite_location")
    @Expose
    private Object worksiteLocation;
    @SerializedName("contact_number")
    @Expose
    private String contactNumber;
    @SerializedName("contact_email")
    @Expose
    private String contactEmail;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("finish_time")
    @Expose
    private String finishTime;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("client_company_name")
    @Expose
    private String clientCompanyName;
    @SerializedName("client_contact_name")
    @Expose
    private String clientContactName;
    @SerializedName("phone_number_on_site")
    @Expose
    private String phoneNumberOnSite;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("job_number")
    @Expose
    private Object jobNumber;
    @SerializedName("po_number")
    @Expose
    private String poNumber;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("roster_status")
    @Expose
    private String rosterStatus;
    @SerializedName("company_name")
    @Expose
    private Object companyName;
    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("resource_requirement_id")
    @Expose
    private String resourceRequirementId;



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Object getWorksiteLocation() {
        return worksiteLocation;
    }

    public void setWorksiteLocation(Object worksiteLocation) {
        this.worksiteLocation = worksiteLocation;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getClientCompanyName() {
        return clientCompanyName;
    }

    public void setClientCompanyName(String clientCompanyName) {
        this.clientCompanyName = clientCompanyName;
    }

    public String getClientContactName() {
        return clientContactName;
    }

    public void setClientContactName(String clientContactName) {
        this.clientContactName = clientContactName;
    }

    public String getPhoneNumberOnSite() {
        return phoneNumberOnSite;
    }

    public void setPhoneNumberOnSite(String phoneNumberOnSite) {
        this.phoneNumberOnSite = phoneNumberOnSite;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Object getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(Object jobNumber) {
        this.jobNumber = jobNumber;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRosterStatus() {
        return rosterStatus;
    }

    public void setRosterStatus(String rosterStatus) {
        this.rosterStatus = rosterStatus;
    }

    public Object getCompanyName() {
        return companyName;
    }

    public void setCompanyName(Object companyName) {
        this.companyName = companyName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResourceRequirementId() {
        return resourceRequirementId;
    }

    public void setResourceRequirementId(String resourceRequirementId) {
        this.resourceRequirementId = resourceRequirementId;
    }

    public String getShiftTitle() {
        return shiftTitle;
    }

    public void setShiftTitle(String shiftTitle) {
        this.shiftTitle = shiftTitle;
    }

    @Override
    public int compareTo(Job secondObject) {
        if(startDate == null || secondObject.startDate == null) return 0;

        try {
            String pattern = "dd/MM/yyyy";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            Date myDate = simpleDateFormat.parse(startDate);
            Date secondDate = simpleDateFormat.parse(secondObject.startDate);
            return secondDate.compareTo(myDate);
        } catch (ParseException e) {
            Log.e("date_formating",e.getMessage());
        }
        return 0;
    }
}
