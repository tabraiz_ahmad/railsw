
package com.blueshield.appsws.models.jobdetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProjectInfo {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("client_id")
    @Expose
    private Integer clientId;
    @SerializedName("booking_id")
    @Expose
    private Integer bookingId;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("contact_number")
    @Expose
    private Object contactNumber;
    @SerializedName("contact_email")
    @Expose
    private String contactEmail;
    @SerializedName("po_number")
    @Expose
    private String poNumber;
    @SerializedName("rail_network")
    @Expose
    private String railNetwork;

    @SerializedName("project_name")
    @Expose
    private String projectName;

    @SerializedName("job_number")
    @Expose
    private String jobNumber;
    @SerializedName("worksite_location")
    @Expose
    private Object worksiteLocation;
    @SerializedName("client_company_name")
    @Expose
    private String clientCompanyName;
    @SerializedName("client_contact_name")
    @Expose
    private String clientContactName;
    @SerializedName("phone_number_on_site")
    @Expose
    private Object phoneNumberOnSite;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("deadline")
    @Expose
    private String deadline;
    @SerializedName("shift_start_time")
    @Expose
    private String shiftStartTime;
    @SerializedName("finish_time")
    @Expose
    private String finishTime;
    @SerializedName("key_project_id")
    @Expose
    private Integer keyProjectId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("notes")
    @Expose
    private String notes;
    @SerializedName("documentation_required")
    @Expose
    private String documentationRequired;
    @SerializedName("special_conditions")
    @Expose
    private String specialConditions;
    @SerializedName("protection_method")
    @Expose
    private String protectionMethod;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("quote_required")
    @Expose
    private String quoteRequired;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("roster_status")
    @Expose
    private String rosterStatus;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("created_by")
    @Expose
    private Integer createdBy;
    @SerializedName("labels")
    @Expose
    private Object labels;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("starred_by")
    @Expose
    private String starredBy;

    @SerializedName("key_project_name")
    @Expose
    private String keyProjectName;


    @SerializedName("deleted")
    @Expose
    private Integer deleted;
    @SerializedName("company_name")
    @Expose
    private Object companyName;
    @SerializedName("currency_symbol")
    @Expose
    private Object currencySymbol;
    @SerializedName("total_points")
    @Expose
    private Object totalPoints;
    @SerializedName("completed_points")
    @Expose
    private Object completedPoints;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Object getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(Object contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    public String getPoNumber() {
        return poNumber;
    }

    public void setPoNumber(String poNumber) {
        this.poNumber = poNumber;
    }

    public String getRailNetwork() {
        return railNetwork;
    }

    public void setRailNetwork(String railNetwork) {
        this.railNetwork = railNetwork;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getJobNumber() {
        return jobNumber;
    }

    public void setJobNumber(String jobNumber) {
        this.jobNumber = jobNumber;
    }

    public Object getWorksiteLocation() {
        return worksiteLocation;
    }

    public void setWorksiteLocation(Object worksiteLocation) {
        this.worksiteLocation = worksiteLocation;
    }

    public String getClientCompanyName() {
        return clientCompanyName;
    }

    public void setClientCompanyName(String clientCompanyName) {
        this.clientCompanyName = clientCompanyName;
    }

    public String getClientContactName() {
        return clientContactName;
    }

    public void setClientContactName(String clientContactName) {
        this.clientContactName = clientContactName;
    }

    public Object getPhoneNumberOnSite() {
        return phoneNumberOnSite;
    }

    public void setPhoneNumberOnSite(Object phoneNumberOnSite) {
        this.phoneNumberOnSite = phoneNumberOnSite;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getShiftStartTime() {
        return shiftStartTime;
    }

    public void setShiftStartTime(String shiftStartTime) {
        this.shiftStartTime = shiftStartTime;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public Integer getKeyProjectId() {
        return keyProjectId;
    }

    public void setKeyProjectId(Integer keyProjectId) {
        this.keyProjectId = keyProjectId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getDocumentationRequired() {
        return documentationRequired;
    }

    public void setDocumentationRequired(String documentationRequired) {
        this.documentationRequired = documentationRequired;
    }

    public String getSpecialConditions() {
        return specialConditions;
    }

    public void setSpecialConditions(String specialConditions) {
        this.specialConditions = specialConditions;
    }

    public String getProtectionMethod() {
        return protectionMethod;
    }

    public void setProtectionMethod(String protectionMethod) {
        this.protectionMethod = protectionMethod;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getQuoteRequired() {
        return quoteRequired;
    }

    public void setQuoteRequired(String quoteRequired) {
        this.quoteRequired = quoteRequired;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRosterStatus() {
        return rosterStatus;
    }

    public void setRosterStatus(String rosterStatus) {
        this.rosterStatus = rosterStatus;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Object getLabels() {
        return labels;
    }

    public void setLabels(Object labels) {
        this.labels = labels;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getStarredBy() {
        return starredBy;
    }

    public void setStarredBy(String starredBy) {
        this.starredBy = starredBy;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Object getCompanyName() {
        return companyName;
    }

    public void setCompanyName(Object companyName) {
        this.companyName = companyName;
    }

    public Object getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(Object currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public Object getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(Object totalPoints) {
        this.totalPoints = totalPoints;
    }

    public Object getCompletedPoints() {
        return completedPoints;
    }

    public void setCompletedPoints(Object completedPoints) {
        this.completedPoints = completedPoints;
    }

    public String getKeyProjectName() {
        return keyProjectName;
    }

    public void setKeyProjectName(String keyProjectName) {
        this.keyProjectName = keyProjectName;
    }
}
