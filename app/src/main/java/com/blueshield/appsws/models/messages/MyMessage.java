package com.blueshield.appsws.models.messages;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by tabraiz on 21/07/18.
 */

public class MyMessage implements Comparable<MyMessage>{
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("main_message_id")
    @Expose
    private Integer mainMessageId;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("status")
    @Expose
    private String status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMainMessageId() {
        return mainMessageId;
    }

    public void setMainMessageId(Integer mainMessageId) {
        this.mainMessageId = mainMessageId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int compareTo(@NonNull MyMessage o) {
        return o.getCreatedAt().compareTo(getCreatedAt());
    }
}
