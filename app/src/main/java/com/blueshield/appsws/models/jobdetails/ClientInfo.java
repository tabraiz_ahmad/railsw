
package com.blueshield.appsws.models.jobdetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ClientInfo {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("zip")
    @Expose
    private String zip;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("currency_symbol")
    @Expose
    private String currencySymbol;
    @SerializedName("starred_by")
    @Expose
    private String starredBy;
    @SerializedName("group_ids")
    @Expose
    private String groupIds;
    @SerializedName("deleted")
    @Expose
    private Integer deleted;
    @SerializedName("abn")
    @Expose
    private String abn;
    @SerializedName("vat_number")
    @Expose
    private String vatNumber;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("disable_online_payment")
    @Expose
    private Integer disableOnlinePayment;
    @SerializedName("primary_contact")
    @Expose
    private String primaryContact;
    @SerializedName("primary_contact_id")
    @Expose
    private Integer primaryContactId;
    @SerializedName("contact_avatar")
    @Expose
    private Object contactAvatar;
    @SerializedName("total_projects")
    @Expose
    private Integer totalProjects;
    @SerializedName("invoice_value")
    @Expose
    private Integer invoiceValue;
    @SerializedName("payment_received")
    @Expose
    private Integer paymentReceived;
    @SerializedName("groups")
    @Expose
    private Object groups;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public String getStarredBy() {
        return starredBy;
    }

    public void setStarredBy(String starredBy) {
        this.starredBy = starredBy;
    }

    public String getGroupIds() {
        return groupIds;
    }

    public void setGroupIds(String groupIds) {
        this.groupIds = groupIds;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public String getAbn() {
        return abn;
    }

    public void setAbn(String abn) {
        this.abn = abn;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getDisableOnlinePayment() {
        return disableOnlinePayment;
    }

    public void setDisableOnlinePayment(Integer disableOnlinePayment) {
        this.disableOnlinePayment = disableOnlinePayment;
    }

    public String getPrimaryContact() {
        return primaryContact;
    }

    public void setPrimaryContact(String primaryContact) {
        this.primaryContact = primaryContact;
    }

    public Integer getPrimaryContactId() {
        return primaryContactId;
    }

    public void setPrimaryContactId(Integer primaryContactId) {
        this.primaryContactId = primaryContactId;
    }

    public Object getContactAvatar() {
        return contactAvatar;
    }

    public void setContactAvatar(Object contactAvatar) {
        this.contactAvatar = contactAvatar;
    }

    public Integer getTotalProjects() {
        return totalProjects;
    }

    public void setTotalProjects(Integer totalProjects) {
        this.totalProjects = totalProjects;
    }

    public Integer getInvoiceValue() {
        return invoiceValue;
    }

    public void setInvoiceValue(Integer invoiceValue) {
        this.invoiceValue = invoiceValue;
    }

    public Integer getPaymentReceived() {
        return paymentReceived;
    }

    public void setPaymentReceived(Integer paymentReceived) {
        this.paymentReceived = paymentReceived;
    }

    public Object getGroups() {
        return groups;
    }

    public void setGroups(Object groups) {
        this.groups = groups;
    }

}