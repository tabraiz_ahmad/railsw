package com.blueshield.appsws.models.jobdetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FileInfo {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("file_name")
    @Expose
    private String fileName;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("file_size")
    @Expose
    private String fileSize;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("project_id")
    @Expose
    private String projectId;
    @SerializedName("uploaded_by")
    @Expose
    private String uploadedBy;
    @SerializedName("deleted")
    @Expose
    private String deleted;
    @SerializedName("uploaded_by_user_name")
    @Expose
    private String uploadedByUserName;
    @SerializedName("uploaded_by_user_image")
    @Expose
    private String uploadedByUserImage;
    @SerializedName("uploaded_by_user_type")
    @Expose
    private String uploadedByUserType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getUploadedBy() {
        return uploadedBy;
    }

    public void setUploadedBy(String uploadedBy) {
        this.uploadedBy = uploadedBy;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getUploadedByUserName() {
        return uploadedByUserName;
    }

    public void setUploadedByUserName(String uploadedByUserName) {
        this.uploadedByUserName = uploadedByUserName;
    }

    public String getUploadedByUserImage() {
        return uploadedByUserImage;
    }

    public void setUploadedByUserImage(String uploadedByUserImage) {
        this.uploadedByUserImage = uploadedByUserImage;
    }

    public String getUploadedByUserType() {
        return uploadedByUserType;
    }

    public void setUploadedByUserType(String uploadedByUserType) {
        this.uploadedByUserType = uploadedByUserType;
    }

}