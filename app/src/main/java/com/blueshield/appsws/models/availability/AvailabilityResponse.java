package com.blueshield.appsws.models.availability;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by tabraiz on 21/07/18.
 */

public class AvailabilityResponse {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("client_id")
    @Expose
    private Object clientId;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("total_hours")
    @Expose
    private Integer totalHours;
    @SerializedName("total_days")
    @Expose
    private Double totalDays;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("leave_type_title")
    @Expose
    private String leaveTypeTitle;
    @SerializedName("leave_type_color")
    @Expose
    private String leaveTypeColor;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Object getClientId() {
        return clientId;
    }

    public void setClientId(Object clientId) {
        this.clientId = clientId;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Integer getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(Integer totalHours) {
        this.totalHours = totalHours;
    }

    public Double getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(Double totalDays) {
        this.totalDays = totalDays;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLeaveTypeTitle() {
        return leaveTypeTitle;
    }

    public void setLeaveTypeTitle(String leaveTypeTitle) {
        this.leaveTypeTitle = leaveTypeTitle;
    }

    public String getLeaveTypeColor() {
        return leaveTypeColor;
    }

    public void setLeaveTypeColor(String leaveTypeColor) {
        this.leaveTypeColor = leaveTypeColor;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
