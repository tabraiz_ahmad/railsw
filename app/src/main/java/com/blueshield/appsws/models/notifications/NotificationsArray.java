
package com.blueshield.appsws.models.notifications;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationsArray {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("notification_title")
    @Expose
    private String notificationTitle;
    @SerializedName("notification_time")
    @Expose
    private String notificationTime;
    @SerializedName("notification_detail")
    @Expose
    private String notificationDetail;
    @SerializedName("notification_read_by_user")
    @Expose
    private String notificationReadByUser;

    @SerializedName("message_reply_sent")
    @Expose
    private String messageReplySent;

    @SerializedName("notification_event_id")
    @Expose
    private String notificationEventId;

    @SerializedName("notification_type")
    @Expose
    private String notificationType;

    public String getMessageReplySent() {
        return messageReplySent;
    }

    public void setMessageReplySent(String messageReplySent) {
        this.messageReplySent = messageReplySent;
    }

    public String getNotificationEventId() {
        return notificationEventId;
    }

    public void setNotificationEventId(String notificationEventId) {
        this.notificationEventId = notificationEventId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getNotificationTitle() {
        return notificationTitle;
    }

    public void setNotificationTitle(String notificationTitle) {
        this.notificationTitle = notificationTitle;
    }

    public String getNotificationTime() {
        return notificationTime;
    }

    public void setNotificationTime(String notificationTime) {
        this.notificationTime = notificationTime;
    }

    public String getNotificationDetail() {
        return notificationDetail;
    }

    public void setNotificationDetail(String notificationDetail) {
        this.notificationDetail = notificationDetail;
    }

    public String getNotificationReadByUser() {
        return notificationReadByUser;
    }

    public void setNotificationReadByUser(String notificationReadByUser) {
        this.notificationReadByUser = notificationReadByUser;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }
}
