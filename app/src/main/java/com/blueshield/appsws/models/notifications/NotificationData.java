
package com.blueshield.appsws.models.notifications;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationData {

    @SerializedName("newNotificationsCount")
    @Expose
    private Integer newNotificationsCount;
    @SerializedName("notifications_array")
    @Expose
    private List<NotificationsArray> notificationsArray = null;

    public Integer getNewNotificationsCount() {
        return newNotificationsCount;
    }

    public void setNewNotificationsCount(Integer newNotificationsCount) {
        this.newNotificationsCount = newNotificationsCount;
    }

    public List<NotificationsArray> getNotificationsArray() {
        return notificationsArray;
    }

    public void setNotificationsArray(List<NotificationsArray> notificationsArray) {
        this.notificationsArray = notificationsArray;
    }

}
