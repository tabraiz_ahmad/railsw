
package com.blueshield.appsws.models.joblists;

import android.util.Log;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class NewJobsApproval implements Comparable<NewJobsApproval> {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("job_id")
    @Expose
    private Integer jobId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("resource_requirement_id")
    @Expose
    private Integer resourceRequirementId;
    @SerializedName("rail_network")
    @Expose
    private String railNetwork;
    @SerializedName("resource_code")
    @Expose
    private String resourceCode;
    @SerializedName("resource_type")
    @Expose
    private String resourceType;
    @SerializedName("company_name")
    @Expose
    private String companyName;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("finish_time")
    @Expose
    private String finishTime;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("shift_title")
    @Expose
    private String shiftTitle;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("project_name")
    @Expose
    private String projectName;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("protection_method")
    @Expose
    private String protectionMethod;


    @SerializedName("key_project_name")
    @Expose
    private String keyProjectName;


    public String getProtectionMethod() {
        return protectionMethod;
    }

    public void setProtectionMethod(String protectionMethod) {
        this.protectionMethod = protectionMethod;
    }

    public String getKeyProjectName() {
        return keyProjectName;
    }

    public void setKeyProjectName(String keyProjectName) {
        this.keyProjectName = keyProjectName;
    }


    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getResourceRequirementId() {
        return resourceRequirementId;
    }

    public void setResourceRequirementId(Integer resourceRequirementId) {
        this.resourceRequirementId = resourceRequirementId;
    }

    public String getRailNetwork() {
        return railNetwork;
    }

    public void setRailNetwork(String railNetwork) {
        this.railNetwork = railNetwork;
    }

    public String getResourceCode() {
        return resourceCode;
    }

    public void setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getLocation() {
        return location;
    }

    public String getShiftTitle() {
        return shiftTitle;
    }

    public void setShiftTitle(String shiftTitle) {
        this.shiftTitle = shiftTitle;
    }


    @Override
    public int compareTo(NewJobsApproval secondObject) {
        if (startDate == null || secondObject.startDate == null) return 0;

        try {
            String pattern = "dd/MM/yyyy";
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            Date myDate = simpleDateFormat.parse(startDate);
            Date secondDate = simpleDateFormat.parse(secondObject.startDate);
            return secondDate.compareTo(myDate);
        } catch (ParseException e) {
            Log.e("date_formating",e.getMessage());
        }
        return 0;
    }
}
