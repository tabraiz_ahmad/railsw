
package com.blueshield.appsws.models.jobdetails;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseJobDetail {

    @SerializedName("project_info")
    @Expose
    private ProjectInfo projectInfo;
    @SerializedName("client_info")
    @Expose
    private ClientInfo clientInfo;

    @SerializedName("project_progress")
    @Expose
    private Integer projectProgress;

    @SerializedName("roster_table_id")
    @Expose
    private Integer rosterTableId;

    @SerializedName("staffing_requirements")
    @Expose
    private List<StaffingRequirement> staffingRequirements = null;
    @SerializedName("equipment_requirements")
    @Expose
    private List<EquipmentRequirement> equipmentRequirements = null;
    @SerializedName("files_data")
    @Expose
    private List<FilesDatum> filesData = null;
    @SerializedName("rostered_staff")
    @Expose
    private List<RosteredStaff> rosteredStaff = null;

    public Boolean getNewJob() {
        return isNewJob;
    }

    public void setNewJob(Boolean newJob) {
        isNewJob = newJob;
    }

    @SerializedName("isNewJob")
    @Expose
    private Boolean isNewJob = null;


    public ProjectInfo getProjectInfo() {
        return projectInfo;
    }

    public void setProjectInfo(ProjectInfo projectInfo) {
        this.projectInfo = projectInfo;
    }

    public ClientInfo getClientInfo() {
        return clientInfo;
    }

    public void setClientInfo(ClientInfo clientInfo) {
        this.clientInfo = clientInfo;
    }


    public Integer getProjectProgress() {
        return projectProgress;
    }

    public void setProjectProgress(Integer projectProgress) {
        this.projectProgress = projectProgress;
    }

    public List<StaffingRequirement> getStaffingRequirements() {
        return staffingRequirements;
    }

    public void setStaffingRequirements(List<StaffingRequirement> staffingRequirements) {
        this.staffingRequirements = staffingRequirements;
    }

    public List<EquipmentRequirement> getEquipmentRequirements() {
        return equipmentRequirements;
    }

    public void setEquipmentRequirements(List<EquipmentRequirement> equipmentRequirements) {
        this.equipmentRequirements = equipmentRequirements;
    }

    public List<FilesDatum> getFilesData() {
        return filesData;
    }

    public void setFilesData(List<FilesDatum> filesData) {
        this.filesData = filesData;
    }

    public List<RosteredStaff> getRosteredStaff() {
        return rosteredStaff;
    }

    public void setRosteredStaff(List<RosteredStaff> rosteredStaff) {
        this.rosteredStaff = rosteredStaff;
    }

    public Integer getRosterTableId() {
        return rosterTableId;
    }

    public void setRosterTableId(Integer rosterTableId) {
        this.rosterTableId = rosterTableId;
    }
}
