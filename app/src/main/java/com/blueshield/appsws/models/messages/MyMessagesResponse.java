package com.blueshield.appsws.models.messages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by tabraiz on 21/07/18.
 */

public class MyMessagesResponse {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("response")
    @Expose
    private List<MyMessage> response = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<MyMessage> getResponse() {
        return response;
    }

    public void setResponse(List<MyMessage> response) {
        this.response = response;
    }
}
