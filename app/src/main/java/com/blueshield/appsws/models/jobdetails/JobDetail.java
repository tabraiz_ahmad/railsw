
package com.blueshield.appsws.models.jobdetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobDetail {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("response")
    @Expose
    private ResponseJobDetail responseJobDetail;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ResponseJobDetail getResponse() {
        return responseJobDetail;
    }

    public void setResponse(ResponseJobDetail response) {
        this.responseJobDetail = response;
    }

}
