
package com.blueshield.appsws.models.messages;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageDetail {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("response")
    @Expose
    private List<MessageDetailResponse> messageDetailResponse = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<MessageDetailResponse> getMessageDetailResponse() {
        return messageDetailResponse;
    }

    public void setMessageDetailResponse(List<MessageDetailResponse> messageDetailResponse) {
        this.messageDetailResponse = messageDetailResponse;
    }

}
