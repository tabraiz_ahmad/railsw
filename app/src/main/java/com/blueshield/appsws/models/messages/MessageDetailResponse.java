
package com.blueshield.appsws.models.messages;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageDetailResponse {

    @SerializedName("id")
    @Expose
    private Object id;
    @SerializedName("main_message_id")
    @Expose
    private Object mainMessageId;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message_time")
    @Expose
    private String messageTime;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("from_user_id")
    @Expose
    private Object fromUserId;
    @SerializedName("to_user_id")
    @Expose
    private Object toUserId;
    @SerializedName("isMyMessage")
    @Expose
    private String isMyMessage;
    @SerializedName("user_image")
    @Expose
    private String userImage;
    @SerializedName("files")
    @Expose
    private String files;

    @SerializedName("attachment_path")
    @Expose
    private String attachmentPath;



    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getMainMessageId() {
        return mainMessageId;
    }

    public void setMainMessageId(Object mainMessageId) {
        this.mainMessageId = mainMessageId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(String messageTime) {
        this.messageTime = messageTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Object getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Object fromUserId) {
        this.fromUserId = fromUserId;
    }

    public Object getToUserId() {
        return toUserId;
    }

    public void setToUserId(Object toUserId) {
        this.toUserId = toUserId;
    }

    public String getIsMyMessage() {
        return isMyMessage;
    }

    public void setIsMyMessage(String isMyMessage) {
        this.isMyMessage = isMyMessage;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getFiles() {
        return files;
    }

    public void setFiles(String files) {
        this.files = files;
    }

    public String getAttachmentPath() {
        return attachmentPath;
    }

    public void setAttachmentPath(String attachmentPath) {
        this.attachmentPath = attachmentPath;
    }
}
