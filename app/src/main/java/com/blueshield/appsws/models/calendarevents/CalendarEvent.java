package com.blueshield.appsws.models.calendarevents;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CalendarEvent {
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("start")
    @Expose
    private String start;
    @SerializedName("end")
    @Expose
    private String end;
    @SerializedName("encrypted_event_id")
    @Expose
    private String encryptedEventId;
    @SerializedName("backgroundColor")
    @Expose
    private String backgroundColor;
    @SerializedName("borderColor")
    @Expose
    private String borderColor;
    @SerializedName("cycle")
    @Expose
    private Integer cycle;
    @SerializedName("non_event")
    @Expose
    private Boolean nonEvent;

    @SerializedName("job_id")
    @Expose
    private Object jobId = 0;

    @SerializedName("resource_requirement_id")
    @Expose
    private Object resourceRequirementId = 0;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Object getJobId() {
        return jobId;
    }

    public void setJobId(Object jobId) {
        this.jobId = jobId;
    }

    public Object getResourceRequirementId() {
        return resourceRequirementId;
    }

    public void setResourceRequirementId(Object resourceRequirementId) {
        this.resourceRequirementId = resourceRequirementId;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getEncryptedEventId() {
        return encryptedEventId;
    }

    public void setEncryptedEventId(String encryptedEventId) {
        this.encryptedEventId = encryptedEventId;
    }

    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public String getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(String borderColor) {
        this.borderColor = borderColor;
    }

    public Integer getCycle() {
        return cycle;
    }

    public void setCycle(Integer cycle) {
        this.cycle = cycle;
    }

    public Boolean getNonEvent() {
        return nonEvent;
    }

    public void setNonEvent(Boolean nonEvent) {
        this.nonEvent = nonEvent;
    }
}
