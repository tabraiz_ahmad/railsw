
package com.blueshield.appsws.models.jobdetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StaffingRequirement {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("project_id")
    @Expose
    private Integer projectId;
    @SerializedName("rail_network")
    @Expose
    private String railNetwork;
    @SerializedName("resource_code")
    @Expose
    private String resourceCode;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("start_date")
    @Expose
    private String startDate;
    @SerializedName("end_date")
    @Expose
    private String endDate;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("end_time")
    @Expose
    private String endTime;
    @SerializedName("include_weekends")
    @Expose
    private String includeWeekends;
    @SerializedName("weekend_only")
    @Expose
    private String weekendOnly;
    @SerializedName("notes")
    @Expose
    private String notes;
    @SerializedName("shift_title")
    @Expose
    private String shiftTitle;

    public String getShiftTitle() {
        return shiftTitle;
    }

    public void setShiftTitle(String shiftTitle) {
        this.shiftTitle = shiftTitle;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getRailNetwork() {
        return railNetwork;
    }

    public void setRailNetwork(String railNetwork) {
        this.railNetwork = railNetwork;
    }

    public String getResourceCode() {
        return resourceCode;
    }

    public void setResourceCode(String resourceCode) {
        this.resourceCode = resourceCode;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getIncludeWeekends() {
        return includeWeekends;
    }

    public void setIncludeWeekends(String includeWeekends) {
        this.includeWeekends = includeWeekends;
    }

    public String getWeekendOnly() {
        return weekendOnly;
    }

    public void setWeekendOnly(String weekendOnly) {
        this.weekendOnly = weekendOnly;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

}
