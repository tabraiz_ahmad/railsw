
package com.blueshield.appsws.models.jobdetails;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FilesDatum {

    @SerializedName("file_url")
    @Expose
    private String fileUrl;
    @SerializedName("file_name")
    @Expose
    private String fileName;
    @SerializedName("is_image_file")
    @Expose
    private Boolean isImageFile;
    @SerializedName("is_google_preview_available")
    @Expose
    private Boolean isGooglePreviewAvailable;
    @SerializedName("file_info")
    @Expose
    private FileInfo fileInfo;

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Boolean getIsImageFile() {
        return isImageFile;
    }

    public void setIsImageFile(Boolean isImageFile) {
        this.isImageFile = isImageFile;
    }

    public Boolean getIsGooglePreviewAvailable() {
        return isGooglePreviewAvailable;
    }

    public void setIsGooglePreviewAvailable(Boolean isGooglePreviewAvailable) {
        this.isGooglePreviewAvailable = isGooglePreviewAvailable;
    }

    public FileInfo getFileInfo() {
        return fileInfo;
    }

    public void setFileInfo(FileInfo fileInfo) {
        this.fileInfo = fileInfo;
    }

}