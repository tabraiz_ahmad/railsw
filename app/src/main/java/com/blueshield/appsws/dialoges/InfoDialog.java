package com.blueshield.appsws.dialoges;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.blueshield.appsws.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InfoDialog extends Dialog {

    private String message;


    DialogButtonsListner listner;
public interface DialogButtonsListner{

    void onPositiveClick();
    void onNegativeClick();

}
    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.infoMessage)
    TextView infoMessage;

    @BindView(R.id.negativeBtn)
    Button negativeBtn;

    @BindView(R.id.positiveBtn)
    Button positiveBtn;

    public InfoDialog(@NonNull Activity mActivity, String message,DialogButtonsListner listner) {
        super(mActivity);
        this.message = message;
        this.listner = listner;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.info_dialog);
        ButterKnife.bind(this);
        infoMessage.setText(message);
        negativeBtn.setOnClickListener(negativeBtnClick);
        positiveBtn.setOnClickListener(positiveBtnClick);
    }

    private View.OnClickListener negativeBtnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            listner.onNegativeClick();
        }
    };

    private View.OnClickListener positiveBtnClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            listner.onPositiveClick();
        }
    };



    public void setMessage(String message){

        this.message = message;
        if(infoMessage != null)
            infoMessage.setText(this.message);
    }

    public void setNegativeButtonText(String text){

        if(negativeBtn != null)
            negativeBtn.setText(text);
    }

    public void setPositiveButtonText(String text){

        if(positiveBtn != null)
            positiveBtn.setText(text);
    }
}
