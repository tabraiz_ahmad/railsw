package com.blueshield.appsws.dialoges;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.blueshield.appsws.R;

public class ErrorDialog extends Dialog {

    private String message;



    @BindView(R.id.txtErrorMessage)
    TextView txtErrorMessage;

    @BindView(R.id.btnError)
    Button btnError;

    public ErrorDialog(@NonNull Activity mActivity, String message) {
        super(mActivity);
        this.message = message;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_error_dialog);
        ButterKnife.bind(this);
        txtErrorMessage.setText(message);
        btnError.setOnClickListener(buttonClose);
    }

    private View.OnClickListener buttonClose = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dismiss();
        }
    };

    public void setMessage(String message){

        this.message = message;
        if(txtErrorMessage != null)
            txtErrorMessage.setText(this.message);
    }
}
