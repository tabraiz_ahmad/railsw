package com.blueshield.appsws.dialoges;

import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.widget.Toolbar;

import com.blueshield.appsws.R;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

public class FullScreenDialog extends DialogFragment implements View.OnClickListener {
    public static final String TAG = "fullscreen_dialog";

    private Toolbar toolbar;
    private Button rejectDialogButton;

    String reasonText;
    EditText reasonET;
    RejectButtonListner listner;

    public void setListner(RejectButtonListner listner) {
        this.listner = listner;
    }

    public static FullScreenDialog display(FragmentManager fragmentManager) {
        FullScreenDialog fullScreenDialog = new FullScreenDialog();
        fullScreenDialog.show(fragmentManager, TAG);
        return fullScreenDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fullscreen_dialog, container, false);

        toolbar = view.findViewById(R.id.toolbar);
        rejectDialogButton = view.findViewById(R.id.rejectDialogButton);

        rejectDialogButton.setOnClickListener(this);
        reasonET = view.findViewById(R.id.reasonBox);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setNavigationOnClickListener(v -> dismiss());
            toolbar.setTitle("Reason For Rejecting");
            toolbar.setOnMenuItemClickListener(item -> {
                dismiss();
                return true;
            });
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setWindowAnimations(R.style.AppTheme_Slide);
        }
    }

    @Override
    public void onClick(View v) {

        if(v.getId() == R.id.rejectDialogButton){
            if(listner != null){
                reasonText = String.valueOf(reasonET.getText());
                if(reasonText.length() > 0){
                    dismiss();
                    listner.onRejectClick(reasonText);
                }
            }
        }
    }
    public interface RejectButtonListner{
        void onRejectClick(String reason);
    }
}
