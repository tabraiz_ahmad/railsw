package com.blueshield.appsws;

import android.app.Application;
import androidx.annotation.NonNull;
import android.util.Log;

import com.blueshield.appsws.netwrok.RestClient;
import com.blueshield.appsws.netwrok.WebService;
import com.blueshield.appsws.preferences.PreferenceManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import java.util.Objects;

public class RailApplication extends Application {

    private static RestClient restClient;

    @Override
    public void onCreate() {
        super.onCreate();
        PreferenceManager.initializeInstance(this);

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        Log.w("TAG", "getInstanceId failed", task.getException());
                        return;
                    }

                    String token = Objects.requireNonNull(task.getResult()).getToken();
                    PreferenceManager.getInstance().setFCMToken(token);

                     Log.d("fcm",token);
                });
    }
    public static WebService getWebService() {
        restClient = RestClient.getInstance();
        return restClient.getService();
    }
}
