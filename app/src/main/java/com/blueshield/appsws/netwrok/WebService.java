package com.blueshield.appsws.netwrok;

import com.blueshield.appsws.models.BaseResponse;
import com.blueshield.appsws.models.availability.AvailabilityResponse;
import com.blueshield.appsws.models.calendarevents.CalendarEvent;
import com.blueshield.appsws.models.contacts.Contact;
import com.blueshield.appsws.models.jobdetails.JobDetail;
import com.blueshield.appsws.models.jobdetails.JobStatusResponse;
import com.blueshield.appsws.models.joblists.JobListResponse;
import com.blueshield.appsws.models.messages.MessageDetail;
import com.blueshield.appsws.models.messages.MyMessagesResponse;
import com.blueshield.appsws.models.notifications.NotificationData;
import com.blueshield.appsws.models.user.LoginResponse;

import java.io.File;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface WebService {

    @GET("login")
    Call<LoginResponse> doLogin(@Query("email") String email,@Query("password") String password,@Query("device_id") String fcmId,@Query("type") String type);

    @GET("get_all_jobs")
    Call<JobListResponse> getAllJobs(@Query("user_id") String userId, @Query("access_token") String accessToken);

    @GET("get_job_details")
    Call<JobDetail> getJobDetail(@Query("user_id") String userId, @Query("access_token") String accessToken, @Query("job_id") int JobId,@Query("resource_requirement_id") int requirementId );

    @GET("get_job_details")
    Call<JobDetail> getJobDetail(@Query("user_id") String userId, @Query("access_token") String accessToken, @Query("job_id") int JobId);

    @GET("change_job_status")
    Call<JobStatusResponse> changeJobStatus(@Query("user_id") String userId, @Query("access_token") String accessToken, @Query("job_id") int JobId, @Query("id") int id, @Query("status") String status);

    @GET("change_job_status")
    Call<JobStatusResponse> changeJobStatusWithReason(@Query("user_id") String userId, @Query("access_token") String accessToken, @Query("job_id") int JobId, @Query("id") int id, @Query("status") String status, @Query("reason") String reason);

    @GET("my_availability")
    Call<BaseResponse<List<AvailabilityResponse>>> getMyAvailability(@Query("user_id") String userId, @Query("access_token") String accessToken);

    @GET("my_messages")
    Call<MyMessagesResponse> getMyMessages(@Query("user_id") String userId, @Query("access_token") String accessToken);

    @GET("logout")
    Call<ResponseBody> logout(@Query("user_id") String userId, @Query("access_token") String accessToken);


    @GET("delete_unavailability")
    Call<ResponseBody> deleteUnavailability(@Query("user_id") String userId, @Query("access_token") String accessToken,@Query("delete_id") Integer deleteId);

    @GET("get_calendar_events")
    Call<BaseResponse<List<CalendarEvent>>> getCalendarEvents(@Query("user_id") String userId, @Query("access_token") String accessToken);

    @GET("get_unavailability_dropdown_data")
    Call<ResponseBody> getUnavailabilityDropDownData(@Query("user_id") String userId, @Query("access_token") String accessToken);

    @GET("get_my_notifications")
    Call<BaseResponse<NotificationData>> getMyNotifications(@Query("user_id") String userId, @Query("access_token") String accessToken);

    @GET("get_contact_list")
    Call<BaseResponse<List<Contact>>> getMyContacts(@Query("user_id") String userId, @Query("access_token") String accessToken);

    @GET("add_unavailability")
    Call<ResponseBody> addUnavailability(@Query("user_id") String userId, @Query("access_token") String accessToken,@Query("leave_type_id") String leaveTypeId,@Query("duration") String duration, @Query("reason") String reason , @QueryMap Map<String, String> dateTimings);

    @GET("view_message")
    Call<MessageDetail> getMessageDetail(@Query("user_id") String userId, @Query("access_token") String accessToken, @Query("main_message_id") String messageId);

    @POST("reply_message")
    Call<ResponseBody> replyMessage(@Query("user_id") String userId, @Query("access_token") String accessToken, @Query("main_message_id") String messageId, @Query("reply_message") String reply);

    @Multipart
    @POST("reply_message")
    Call<ResponseBody> replyMessage(@Query("user_id") String userId, @Query("access_token") String accessToken, @Query("main_message_id") String messageId, @Query("reply_message") String reply, @Part MultipartBody.Part file);


    @POST("create_message")
    Call<ResponseBody> createMessage(@Query("user_id") String userId, @Query("access_token") String accessToken, @Query("message") String message, @Query("to_user_id") String toUserId);

    @Multipart
    @POST("create_message")
    Call<ResponseBody> createMessage(@Query("user_id") String userId, @Query("access_token") String accessToken, @Query("message") String message,@Query("to_user_id") String toUserId, @Part MultipartBody.Part file);


}
