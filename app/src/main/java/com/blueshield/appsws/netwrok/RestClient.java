package com.blueshield.appsws.netwrok;


import android.util.Log;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.blueshield.appsws.BuildConfig.BASE_URL;
import static com.blueshield.appsws.common.Constants.TIMEOUT;

public class RestClient {

    private static String WEBSERVICES_URL = BASE_URL + "webservices/";


    private final WebService apiService;
    private final HttpLoggingInterceptor logging;
    private static RestClient mInstance;
    private Retrofit mRetrofit;

    private RestClient() {


         logging = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override public void log(String message) {
                Log.e("OKHTTP",message);
            }
        });
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(logging)
                .build();

        mRetrofit = new Retrofit.Builder()
                .baseUrl(WEBSERVICES_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        apiService = mRetrofit.create(WebService.class);

    }

    public static RestClient getInstance(){
        if(mInstance == null){
            mInstance = new RestClient();
        }
        return mInstance;
    }
    public WebService getService() {
        return apiService;
    }

}
