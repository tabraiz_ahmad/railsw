package com.blueshield.appsws.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.blueshield.appsws.R;
import com.blueshield.appsws.RailApplication;
import com.blueshield.appsws.adapters.NotificationListAdapter;
import com.blueshield.appsws.decorators.DividerItemDecoration;
import com.blueshield.appsws.dialoges.ErrorDialog;
import com.blueshield.appsws.models.BaseResponse;
import com.blueshield.appsws.models.notifications.NotificationData;
import com.blueshield.appsws.models.notifications.NotificationsArray;
import com.blueshield.appsws.preferences.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.blueshield.appsws.common.Constants.JOB_ID;
import static com.blueshield.appsws.common.Constants.MESSAGE_ID;

public class NotificationListActivity extends AppCompatActivity implements NotificationListAdapter.NotificationClickListner {

    @BindView(R.id.notificationList)
    RecyclerView notificationList;

    @BindView(R.id.progressBar)
    ProgressBar mProgressView;

    @BindView(R.id.emptyTextView)
    TextView emptyTextView;

    protected ErrorDialog errorDialog;

    List<NotificationsArray> notificationsArray = new ArrayList<>();
    NotificationListAdapter notificationListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications_list);
        setTitle(getString(R.string.notifications));
        ButterKnife.bind(this);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
            actionBar.setDisplayHomeAsUpEnabled(true);
        notificationListAdapter = new NotificationListAdapter(notificationsArray, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        notificationList.setLayoutManager(mLayoutManager);
        notificationList.addItemDecoration(new DividerItemDecoration(notificationList.getContext()));
        notificationList.setAdapter(notificationListAdapter);
        fetchNotificationsList();
        errorDialog = new ErrorDialog(this, getString(R.string.went_wrong));

    }

    private void fetchNotificationsList() {

        showProgress(true);
        String id = PreferenceManager.getInstance().getId();
        String user_token = PreferenceManager.getInstance().getToken();
        Call<BaseResponse<NotificationData>> call = RailApplication.getWebService().getMyNotifications(id, user_token);
        call.enqueue(new Callback<BaseResponse<NotificationData>>() {
            @Override
            public void onResponse(Call<BaseResponse<NotificationData>> call, Response<BaseResponse<NotificationData>> response) {
                showProgress(false);
                BaseResponse<NotificationData> myNotificationResponse = response.body();
                NotificationData notificationData = (NotificationData) myNotificationResponse.getResponse();
                notificationListAdapter.setNotificationsArray(notificationData.getNotificationsArray());
                notificationListAdapter.notifyDataSetChanged();

                if (notificationData.getNotificationsArray() == null || notificationData.getNotificationsArray().size() == 0) {
                    emptyTextView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<NotificationData>> call, Throwable t) {
                showProgress(false);
                errorDialog.show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (!isTaskRoot()) {
                    finish();
                } else {
                    callMainActivity();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
        if (show) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    public void callMainActivity() {
        Intent mainActivityIntent = new Intent(this, MainActivity.class); //Show second fragment
        startActivity(mainActivityIntent);
        finish();
    }

    @Override
    public void onNotificationRowClick(NotificationsArray notificationsArray) {
        if (notificationsArray.getNotificationType().equalsIgnoreCase("new_message_sent" ) || notificationsArray.getNotificationType().equalsIgnoreCase("message_reply_sent")) {
              openMessageDetail(notificationsArray.getNotificationEventId());
        } else if (notificationsArray.getNotificationType().equalsIgnoreCase("job_assigned_for_approval") || notificationsArray.getNotificationType().equalsIgnoreCase("booking_submitted_for_approval") ) {
            openJobDetail(notificationsArray.getNotificationEventId());
        }
    }

    private void openJobDetail(String notificationEventId) {

        try{

            Intent intent = new Intent(this, JobDetailActivity.class);
            intent.putExtra(JOB_ID, Integer.parseInt(notificationEventId));

            startActivity(intent);

        }catch (NumberFormatException e){

            Log.e("Notification_Id",e.getMessage());
        }

    }

    private void openMessageDetail(String notificationEventId) {

        Intent intent = new Intent(this, MessagesDetailActivity.class);
        intent.putExtra(MESSAGE_ID, notificationEventId);
        startActivity(intent);
    }
}
