package com.blueshield.appsws.activities

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.TargetApi
import android.content.Intent
import android.graphics.Color
import android.graphics.RectF
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import butterknife.ButterKnife
import com.alamkanak.weekview.DateTimeInterpreter
import com.alamkanak.weekview.MonthLoader
import com.alamkanak.weekview.WeekView
import com.alamkanak.weekview.WeekViewEvent
import com.blueshield.appsws.R
import com.blueshield.appsws.RailApplication
import com.blueshield.appsws.common.Constants.JOB_ID
import com.blueshield.appsws.common.Constants.RESOURCE_ID
import com.blueshield.appsws.dialoges.ErrorDialog
import com.blueshield.appsws.models.BaseResponse
import com.blueshield.appsws.models.calendarevents.CalendarEvent
import com.blueshield.appsws.preferences.PreferenceManager
import kotlinx.android.synthetic.main.activity_calander.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.NumberFormatException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class CalanderActivity : AppCompatActivity(), WeekView.EventClickListener, MonthLoader.MonthChangeListener {


    lateinit var errorDialog: ErrorDialog

    private var mWeekViewType = TYPE_THREE_DAY_VIEW


    // Populate the week view with some events.
    private lateinit var events: MutableList<WeekViewEvent>

    lateinit var calendarEvents: List<CalendarEvent>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calander)
        setTitle(R.string.calendar)
        ButterKnife.bind(this)
        val actionBar = supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)


        // The week view has infinite scrolling horizontally. We have to provide the events of a
        // month every time the month changes on the week view.
        mWeekView.monthChangeListener = this
        mWeekView.setLimitTime(0, 24)

        mWeekView.eventClickListener = this

        events = ArrayList()
        setupDateTimeInterpreter(false)
        fetchEventsList()
    }

    private fun fetchEventsList() {
        showProgress(true)
        val id = PreferenceManager.getInstance().id
        val accessToken = PreferenceManager.getInstance().token
        errorDialog = ErrorDialog(this, getString(R.string.went_wrong))
        val call = RailApplication.getWebService().getCalendarEvents(id, accessToken)
        call.enqueue(object : Callback<BaseResponse<List<CalendarEvent>>> {
            override fun onResponse(call: Call<BaseResponse<List<CalendarEvent>>>, response: Response<BaseResponse<List<CalendarEvent>>>) {
                showProgress(false)
                val baseResponse = response.body()
                if (baseResponse != null) {
                    calendarEvents = baseResponse.response
                    makeMWeekView(calendarEvents)
                    mWeekView.notifyDataSetChanged()

                    //                    mWeekView.colu
                }
            }

            override fun onFailure(call: Call<BaseResponse<List<CalendarEvent>>>, t: Throwable) {
                showProgress(false)
                errorDialog.show()
            }
        })
    }

    private fun makeMWeekView(calendarEvents: List<CalendarEvent>): List<WeekViewEvent>? {
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)

        //        2018-08-11 22:00:00
        for (position in calendarEvents.indices) {

            val cEvent = calendarEvents[position]

            val startTime = Calendar.getInstance()
            val endTime = startTime.clone() as Calendar
            try {
                startTime.time = sdf.parse(cEvent.start)
                endTime.time = sdf.parse(cEvent.end)
            } catch (e: ParseException) {
                e.printStackTrace()
            }

            // Create an week view event.
            val weekViewEvent = WeekViewEvent(position.toString(), cEvent.title, startTime, endTime)
            weekViewEvent.color = Color.parseColor(cEvent.backgroundColor)

            events.add(weekViewEvent)

        }
        return events
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.calendar_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        setupDateTimeInterpreter(id == R.id.action_week_view)
        when (id) {
            R.id.action_today -> {
                mWeekView.goToToday()
                return true
            }
            R.id.action_day_view -> {
                if (mWeekViewType != TYPE_DAY_VIEW) {
                    item.isChecked = !item.isChecked
                    mWeekViewType = TYPE_DAY_VIEW
                    mWeekView.numberOfVisibleDays = 1

                    // Lets change some dimensions to best fit the view.
                    mWeekView.columnGap = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8.0F, resources.displayMetrics).toInt()
                    mWeekView.textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12.0F, resources.displayMetrics)
                    mWeekView.eventTextSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12.0F, resources.displayMetrics)
                }
                return true
            }
            R.id.action_three_day_view -> {
                if (mWeekViewType != TYPE_THREE_DAY_VIEW) {
                    item.isChecked = !item.isChecked
                    mWeekViewType = TYPE_THREE_DAY_VIEW
                    mWeekView.numberOfVisibleDays = 3

                    // Lets change some dimensions to best fit the view.
                    mWeekView.columnGap = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8f, resources.displayMetrics).toInt()
                    mWeekView.textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12f, resources.displayMetrics)
                    mWeekView.eventTextSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12f, resources.displayMetrics)
                }
                return true
            }
            R.id.action_week_view -> {
                if (mWeekViewType != TYPE_WEEK_VIEW) {
                    item.isChecked = !item.isChecked
                    mWeekViewType = TYPE_WEEK_VIEW
                    mWeekView.numberOfVisibleDays = 7

                    // Lets change some dimensions to best fit the view.
                    mWeekView.columnGap = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8f, resources.displayMetrics).toInt()
                    mWeekView.textSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10f, resources.displayMetrics)
                    mWeekView.eventTextSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10f, resources.displayMetrics)
                    mWeekView.headerWeekDayTitleTextSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 8f, resources.displayMetrics)

                }
                return true
            }

            android.R.id.home -> {
                if (!isTaskRoot) {
                    finish()
                } else {
                    callMainActivity()
                }
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private fun showProgress(show: Boolean) {
        val shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime)
        mProgressView.visibility = if (show) View.VISIBLE else View.GONE
        mProgressView.animate().setDuration(shortAnimTime.toLong()).alpha(
                (if (show) 1 else 0).toFloat()).setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                mProgressView.visibility = if (show) View.VISIBLE else View.GONE
            }
        })
        if (show) {
            window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        } else {
            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
    }

    private fun callMainActivity() {
        val mainActivityIntent = Intent(this, MainActivity::class.java) //Show second fragment
        startActivity(mainActivityIntent)
        finish()
    }

    override fun onMonthChange(newYear: Int, newMonth: Int): MutableList<out WeekViewEvent>? {
        // Return only the events that matches newYear and newMonth.
        val matchedEvents = ArrayList<WeekViewEvent>()

            for (event in events) {
                if (eventMatches(event, newYear, newMonth)) {
                    matchedEvents.add(event)
                }
            }
        return matchedEvents
    }

    /**
     * Checks if an event falls into a specific year and month.
     * @param event The event to check for.
     * @param year The year.
     * @param month The month.
     * @return True if the event matches the year and month.
     */
    private fun eventMatches(event: WeekViewEvent, year: Int, month: Int): Boolean {
        return try {
            event.startTime.get(Calendar.YEAR) == year && event.startTime.get(Calendar.MONTH) == month - 1 || event.endTime.get(Calendar.YEAR) == year && event.endTime.get(Calendar.MONTH) == month - 1
        } catch (e: NullPointerException) {
            //makeMWeekView(calendarEvents);
            false
        }

    }


    override fun onEventClick(event: WeekViewEvent, eventRect: RectF) {


        event.id?.toInt()?.let { position ->

            val cEvent = calendarEvents[position]
            try {
                openJobDetail(jobId = (cEvent.jobId as Double).toInt(),resourceId = (cEvent.resourceRequirementId as Double).toInt())
            }catch (e: ClassCastException){
                Log.d("eventClick",e.message)
            }

        }
    }

    private fun openJobDetail(jobId: Int, resourceId: Int) {
        val intent = Intent(this, JobDetailActivity::class.java)
        intent.putExtra(JOB_ID, jobId)
        intent.putExtra(RESOURCE_ID, resourceId)
        startActivity(intent)
    }

    /**
     * Set up a date time interpreter which will show short date values when in week view and long
     * date values otherwise.
     * @param shortDate True if the date values should be short.
     */
    private fun setupDateTimeInterpreter(shortDate: Boolean) {
        mWeekView.dateTimeInterpreter = object : DateTimeInterpreter {
            override fun getFormattedWeekDayTitle(date: Calendar): String {
                val weekdayNameFormat = SimpleDateFormat("EEE", Locale.getDefault())
                var weekday = weekdayNameFormat.format(date.time)
                val format = SimpleDateFormat(" d/M/yy", Locale.getDefault())

                if (shortDate)
                    weekday = weekday[0].toString()
                return weekday.toUpperCase() + format.format(date.time)
            }

            override fun getFormattedTimeOfDay(hour: Int, minutes: Int): String {
                return if (hour > 9) "$hour:00" else "0$hour:00"

            }
        }
    }

    companion object {

        private const val TYPE_DAY_VIEW = 1
        private const val TYPE_THREE_DAY_VIEW = 2
        private const val TYPE_WEEK_VIEW = 3
    }

}