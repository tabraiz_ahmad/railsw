package com.blueshield.appsws.activities

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.TargetApi
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import android.view.WindowManager
import butterknife.ButterKnife
import com.blueshield.appsws.R
import com.blueshield.appsws.RailApplication
import com.blueshield.appsws.common.Constants.*
import com.blueshield.appsws.dialoges.ErrorDialog
import com.blueshield.appsws.models.user.LoginResponse
import com.blueshield.appsws.preferences.PreferenceManager
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

/**
 * A login screen that offers login via email/password.
 */
class LoginActivity : BaseActivity() {




    private var loginResponse: LoginResponse? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        ButterKnife.bind(this)
        val extras = intent.extras

        mEmailSignInButton?.setOnClickListener { attemptLogin() }

        errorDialog = ErrorDialog(this, getString(R.string.invalid_email_password))


        if (!PreferenceManager.getInstance().tokenIsEmpty()) {

            val isFromURI = intent?.data?.path?.contains("job_detail")
            if (isFromURI != null && isFromURI) {
                openJobDetailByLink()
            } else {

                var keySet: Set<String>? = null
                if (extras != null) {
                    keySet = extras.keySet()
                }
                var intent: Intent
                if (keySet != null && keySet.contains(KEY_MESSAGE_INFO)) {
                    intent = Intent(this, MessagesDetailActivity::class.java)
                    try {

                        val jsonObject = JSONObject(Objects.requireNonNull(extras?.get(KEY_MESSAGE_INFO)).toString())
                        if (jsonObject.has("type") && jsonObject.get("type") == "job") {
                            intent = Intent(this, JobDetailActivity::class.java)
                            val jobId = Integer.parseInt(jsonObject.get("project_id").toString())
                            val resourceId = Integer.parseInt(jsonObject.get("resource_requirement_id").toString())
                            intent.putExtra(JOB_ID, jobId)
                            intent.putExtra(RESOURCE_ID, resourceId)

                        } else {
                            intent.putExtra(MESSAGE_ID, jsonObject.get("main_message_id").toString())
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }

                    startActivity(intent)
                    finish()
                } else {
                    callMainActivity()
                }
            }

        }

    }

    private fun openJobDetailByLink() {

        intent?.data?.apply {
            val i = Intent(applicationContext, JobDetailActivity::class.java)
            var jobId = 0
            var resourceId = 0
            getQueryParameter("job_id")?.apply { jobId = toInt() }
            getQueryParameter("resource_requirement_id")?.apply { resourceId = toInt() }
            i.putExtra(JOB_ID, jobId)
            i.putExtra(RESOURCE_ID, resourceId)
            startActivity(i)
            finish()
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private fun attemptLogin() {

        // Reset errors.
        mEmailView?.error = null
        mPasswordView?.error = null

        // Store values at the time of the login attempt.
        val email = mEmailView?.text.toString()
        val password = mPasswordView?.text.toString()

        var cancel = false
        var focusView: View? = null

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView?.error = getString(R.string.error_field_required)
            focusView = mPasswordView
            cancel = true
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView?.error = getString(R.string.error_field_required)
            focusView = mEmailView
            cancel = true
        } else if (!isEmailValid(email)) {
            mEmailView?.error = getString(R.string.error_invalid_email)
            focusView = mEmailView
            cancel = true
        }

        if (cancel) {
            focusView?.requestFocus()
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true)
            sendLoginRequest(email, password)
        }
    }

    private fun sendLoginRequest(email: String, passwordString: String) {
        val fcmToken = PreferenceManager.getInstance().fcmToken
        val call = RailApplication.getWebService().doLogin(email, passwordString, fcmToken, "android")
        call.enqueue(object : Callback<LoginResponse> {
            override fun onResponse(call: Call<LoginResponse>, response: Response<LoginResponse>) {
                showProgress(false)
                loginResponse = response.body()

                if (loginResponse != null && loginResponse?.status == "success") {
                    val user = loginResponse?.response
                    PreferenceManager.getInstance().saveProfile(user?.firstName, user?.lastName, user?.email, user?.accessToken, user?.id?.toString())
                    callMainActivity()

                }
            }

            override fun onFailure(call: Call<LoginResponse>, t: Throwable) {
                showProgress(false)
                errorDialog.show()
            }
        })
    }

    private fun isEmailValid(email: String): Boolean {
        val pattern = Patterns.EMAIL_ADDRESS
        return pattern.matcher(email).matches()
    }


    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private fun showProgress(show: Boolean) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        val shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime)
        mProgressView?.visibility = if (show) View.VISIBLE else View.GONE
        mProgressView?.animate()?.setDuration(shortAnimTime.toLong())?.alpha(
                (if (show) 1 else 0).toFloat())?.setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                mProgressView?.visibility = if (show) View.VISIBLE else View.GONE
            }
        })
        if (show) {
            window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        } else {
            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
    }

    override fun callMainActivity() {
        val mainActivityIntent = Intent(this, MainActivity::class.java) //Show second fragment
        startActivity(mainActivityIntent)
        finish()
    }

    companion object {
        val KEY_MESSAGE_INFO = "message_info"
    }
}

