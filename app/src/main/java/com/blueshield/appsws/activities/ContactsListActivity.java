package com.blueshield.appsws.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.blueshield.appsws.R;
import com.blueshield.appsws.RailApplication;
import com.blueshield.appsws.adapters.ContactsListAdapter;
import com.blueshield.appsws.decorators.DividerItemDecoration;
import com.blueshield.appsws.dialoges.ErrorDialog;
import com.blueshield.appsws.models.BaseResponse;
import com.blueshield.appsws.models.contacts.Contact;
import com.blueshield.appsws.preferences.PreferenceManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.blueshield.appsws.common.Constants.USER_NAME_ARRAY;

public class ContactsListActivity extends AppCompatActivity implements ContactsListAdapter.ContactClickListner {

    public static final String USER_ID_ARRAY_KEY = "user_id";
    @BindView(R.id.contactList)
    RecyclerView recyclerView;

    @BindView(R.id.progressBar)
    ProgressBar mProgressView;

    @BindView(R.id.emptyTextView)
    TextView emptyTextView;

    protected ErrorDialog errorDialog;

    @BindView(R.id.search)
    EditText searchET;

    @BindView(R.id.btnOK)
    Button btnOK;

    List<Contact> contactList = new ArrayList<>();
    ContactsListAdapter contactsListAdapter;
    private ArrayList<Integer> ID_ARRAY = new ArrayList<>();
    private ArrayList<String> NAMES_ARRAY = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts_list);
        setTitle(getString(R.string.contacts));
        ButterKnife.bind(this);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
            actionBar.setDisplayHomeAsUpEnabled(true);
        contactsListAdapter = new ContactsListAdapter(contactList, this,getCallingActivity() != null);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext()));
        recyclerView.setAdapter(contactsListAdapter);
        fetchContactsList();
        errorDialog = new ErrorDialog(this, getString(R.string.went_wrong));
        searchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    contactsListAdapter.getFilter().filter(v.getText());
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(),
                            InputMethodManager.RESULT_UNCHANGED_SHOWN);
                    return true;
                }
                return false;
            }
        });

    }

    private void fetchContactsList() {

        showProgress(true);
        String id = PreferenceManager.getInstance().getId();
        String user_token = PreferenceManager.getInstance().getToken();
        Call<BaseResponse<List<Contact>>> call = RailApplication.getWebService().getMyContacts(id, user_token);
        call.enqueue(new Callback<BaseResponse<List<Contact>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<Contact>>> call, Response<BaseResponse<List<Contact>>> response) {
                showProgress(false);
                BaseResponse<List<Contact>> myContactsResponse = response.body();
                List<Contact> contactList = myContactsResponse.getResponse();
                Collections.sort(contactList);
                contactsListAdapter.setContactsArray(contactList);
                contactsListAdapter.notifyDataSetChanged();

                if (contactList == null || contactList.size() == 0) {
                    emptyTextView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<BaseResponse<List<Contact>>> call, Throwable t) {
                showProgress(false);
                errorDialog.show();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (!isTaskRoot()) {
                    finish();
                } else {
                    callMainActivity();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
        if (show) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    public void callMainActivity() {
        Intent mainActivityIntent = new Intent(this, MainActivity.class); //Show second fragment
        startActivity(mainActivityIntent);
        finish();
    }


    @Override
    public void onContactRowClick(Contact contact) {
        if(getCallingActivity() != null){
            if(contact.isChecked()){
                contact.setChecked(false);
                ID_ARRAY.remove(contact.getId());
                NAMES_ARRAY.remove(contact.getFirstName() + " " + contact.getLastName());
                contactsListAdapter.notifyDataSetChanged();
            }else{
                ID_ARRAY.add( contact.getId() );
                NAMES_ARRAY.add(contact.getFirstName() + " " + contact.getLastName());
                contact.setChecked(true);
                contactsListAdapter.notifyDataSetChanged();
            }
            if(ID_ARRAY.size()>0){
                btnOK.setVisibility(View.VISIBLE);
            }else{
                btnOK.setVisibility(View.INVISIBLE);
            }

        }else{
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + contact.getPhone()));
            startActivity(intent);
        }

    }
    @OnClick(R.id.btnOK)
    public void btnOKayClicked(){

        Intent resultIntent = new Intent();
        resultIntent.putIntegerArrayListExtra(USER_ID_ARRAY_KEY, ID_ARRAY);
        resultIntent.putStringArrayListExtra(USER_NAME_ARRAY, NAMES_ARRAY);
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }
}
