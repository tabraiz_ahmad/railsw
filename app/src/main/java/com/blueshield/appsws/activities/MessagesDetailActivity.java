package com.blueshield.appsws.activities;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.blueshield.appsws.R;
import com.blueshield.appsws.RailApplication;
import com.blueshield.appsws.adapters.MessageDetailAdapter;
import com.blueshield.appsws.common.Constants;
import com.blueshield.appsws.common.PathUtil;
import com.blueshield.appsws.common.Utils;
import com.blueshield.appsws.dialoges.ErrorDialog;
import com.blueshield.appsws.dialoges.InfoDialog;
import com.blueshield.appsws.models.messages.MessageDetail;
import com.blueshield.appsws.models.messages.MessageDetailResponse;
import com.blueshield.appsws.otto.BusProvider;
import com.blueshield.appsws.otto.MessageEvent;
import com.blueshield.appsws.preferences.PreferenceManager;
import com.squareup.otto.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.blueshield.appsws.common.Constants.NOTIFICATION_ID;
import static com.blueshield.appsws.common.Constants.NOTIFICATION_TAG;
import static com.blueshield.appsws.common.Constants.USER_NAME;
import static com.blueshield.appsws.common.Constants.USER_NAME_ARRAY;

public class MessagesDetailActivity extends AppCompatActivity {

    private static final int PICK_IMAGE_CAMERA = 11;
    private static final int PICK_IMAGE_GALLERY = 12;
    @BindView(R.id.progressBar)
    ProgressBar mProgressView;

    private String messageId;
    private ArrayList<Integer> userIDArrayList = new ArrayList<>();

    @BindView(R.id.sendBtn)
    Button sendBtn;

    @BindView(R.id.showAttachment)
    Button showAttachment;

    @BindView(R.id.chatboxET)
    EditText chatboxET;

    @BindView(R.id.messageList)
    RecyclerView messageListView;

    @BindView(R.id.btnAttachment)
    ImageButton btnAttachment;

    private List<MessageDetailResponse> messageDetailResponses = new ArrayList<>();

    private MessageDetailAdapter mMessageAdapter;

    protected ErrorDialog errorDialog;

    private String filePath;
    private Uri fileUri;
    private InfoDialog infoDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages_detail);
        ButterKnife.bind(this);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
            actionBar.setDisplayHomeAsUpEnabled(true);
        messageId = getIntent().getStringExtra(Constants.MESSAGE_ID);
        userIDArrayList = getIntent().getIntegerArrayListExtra(ContactsListActivity.USER_ID_ARRAY_KEY);

        String userName = getIntent().getStringExtra(USER_NAME);
        ArrayList<String> userNameArray = getIntent().getStringArrayListExtra(USER_NAME_ARRAY);

        if (userName != null && userName.length() > 0)
            setTitle(userName);

        if(userNameArray != null && userNameArray.size() > 0 ){
            String title = android.text.TextUtils.join(",", userNameArray);
            setTitle(title);
        }
        if (messageId != null)
            fetchMessageDetail(messageId);
        if (userIDArrayList != null && userIDArrayList.size() > 0) {
            showProgress(false);
        }

        mMessageAdapter = new MessageDetailAdapter(messageDetailResponses);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setStackFromEnd(true);

        messageListView.setLayoutManager(linearLayoutManager);
        messageListView.setAdapter(mMessageAdapter);
        errorDialog = new ErrorDialog(this, getString(R.string.went_wrong));
        Utils.verifyStoragePermissions(this);

    }

    private void fetchMessageDetail(String messageId) {
        showProgress(true);
        String id = PreferenceManager.getInstance().getId();
        String user_token = PreferenceManager.getInstance().getToken();
        Call<MessageDetail> call = RailApplication.getWebService().getMessageDetail(id, user_token, messageId);
        //noinspection NullableProblems
        call.enqueue(new Callback<MessageDetail>() {
            @Override
            public void onResponse(Call<MessageDetail> call, Response<MessageDetail> response) {
                showProgress(false);
                MessageDetail messageDetail = response.body();
                if (messageDetail != null) {
                    messageDetailResponses = (messageDetail.getMessageDetailResponse());
                    mMessageAdapter.setMessageList(messageDetailResponses);
                    MessageDetailResponse messageDetailFirst = messageDetailResponses.get(0);
                    if(messageDetailFirst != null && messageDetailFirst.getUserName() != null)
                        setTitle(messageDetailFirst.getUserName());
                    mMessageAdapter.notifyDataSetChanged();
                    messageListView.smoothScrollToPosition(messageDetailResponses.size());

                }
            }

            @Override
            public void onFailure(Call<MessageDetail> call, Throwable t) {
                showProgress(false);
                errorDialog.show();
            }
        });
    }

    @Override protected void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override protected void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (!isTaskRoot()) {
                    finish();
                } else {
                    callMainActivity();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
        if (show) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    @OnClick(R.id.sendBtn)
    void sendMessage() {

        String message = chatboxET.getText().toString();

        if (message.length() > 0) {
            try {
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            } catch (Exception ignored) {
            }

            String id = PreferenceManager.getInstance().getId();
            String user_token = PreferenceManager.getInstance().getToken();

            Call<ResponseBody> call;
            if (filePath != null && filePath.length() > 0) {
                final File fileToUpload = new File(filePath);
                // create RequestBody instance from file
                RequestBody requestFile =
                        RequestBody.create(
                                MediaType.parse(Utils.getMimeType(filePath)),
                                fileToUpload
                        );

                // MultipartBody.Part is used to send also the actual file name
                MultipartBody.Part body =
                        MultipartBody.Part.createFormData("file", fileToUpload.getName(), requestFile);

                if (messageId == null || messageId.length() < 1) {
                    call = RailApplication.getWebService().createMessage(id, user_token, message,
                            android.text.TextUtils.join(",", userIDArrayList) , body);
                } else {
                    call = RailApplication.getWebService().replyMessage(id, user_token, messageId, message, body);
                }
            } else {
                if (messageId == null || messageId.length() < 1) {
                    call = RailApplication.getWebService().createMessage(id, user_token, message,
                            android.text.TextUtils.join(",", userIDArrayList) );
                } else {
                    call = RailApplication.getWebService().replyMessage(id, user_token, messageId, message);
                }
            }
            showProgress(true);
            chatboxET.setText("");

            //noinspection NullableProblems
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (messageId != null && messageId.length() > 0) {
                        fetchMessageDetail(messageId);
                    } else {
                        Toast.makeText(MessagesDetailActivity.this, getString(R.string.message_sent), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    filePath = "";
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    showProgress(false);
                    filePath = "";
                    errorDialog.show();
                }
            });
        }

    }

    @OnClick(R.id.btnAttachment)
    void showPictureDialog() {

        try {
            PackageManager pm = getPackageManager();
            int hasPerm = pm.checkPermission(Manifest.permission.CAMERA, getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {
                final CharSequence[] options = {getString(R.string.take_photo), getString(R.string.from_device), "Cancel"};
                androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(this);
                builder.setTitle(R.string.select_option);
                builder.setItems(options, (dialog, item) -> {
                    dialog.dismiss();
                    if (item == 0) {
                        startCameraActivity();
                    } else if (item == 1) {
                        openGallery();
                    }
                });
                builder.show();
            } else
                Toast.makeText(this, R.string.camera_permission_error, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(this, R.string.camera_permission_error, Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @OnClick(R.id.showAttachment)
    void showAttachmentData(){

        infoDialog = new InfoDialog(this, "Selected File: " + filePath, new InfoDialog.DialogButtonsListner() {
            @Override
            public void onPositiveClick() {
                if(infoDialog.isShowing())
                    infoDialog.dismiss();
            }

            @Override
            public void onNegativeClick() {
                filePath = "";
                fileUri = null;
                showAttachment.setVisibility(View.INVISIBLE);
                if(infoDialog.isShowing())
                    infoDialog.dismiss();
            }
        });

        infoDialog.show();
        infoDialog.setNegativeButtonText("Remove Attachment");

    }
    private void openGallery() {
        //new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        Intent pickPhoto = getFileChooserIntent();
        startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);
    }

    private Intent getFileChooserIntent() {
        String[] mimeTypes = {"image/*", "application/pdf", "application/msword"};

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.setType("*/*");
            intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        } else {
            String mimeTypesStr = "";
            for (String mimeType : mimeTypes) {
                mimeTypesStr += mimeType + "|";
            }
            intent.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
        }
        return intent;
    }

    private void startCameraActivity() {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = getOutputMediaFileUri(1);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, PICK_IMAGE_CAMERA);
    }

    public void callMainActivity() {
        Intent mainActivityIntent = new Intent(this, MainActivity.class); //Show second fragment
        startActivity(mainActivityIntent);
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (requestCode == PICK_IMAGE_GALLERY) {
            if (data != null) {
                fileUri = data.getData();
                filePath = PathUtil.getPath(this, fileUri);
                showAttachment.setVisibility(View.VISIBLE);
            }

        } else if (requestCode == PICK_IMAGE_CAMERA) {
            if (resultCode == RESULT_OK) {
                filePath = fileUri.getPath();
                showAttachment.setVisibility(View.VISIBLE);
            }
        }
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(Utils.getOutputMediaFile(type));
    }

    @Subscribe
    public void onMessage(MessageEvent event) {
        fetchMessageDetail(event.messageId);

    }

}
