package com.blueshield.appsws.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.blueshield.appsws.R;
import com.blueshield.appsws.RailApplication;
import com.blueshield.appsws.adapters.EquipmentListAdapter;
import com.blueshield.appsws.adapters.FilesListAdapter;
import com.blueshield.appsws.adapters.RoastedListAdapter;
import com.blueshield.appsws.common.Utils;
import com.blueshield.appsws.dialoges.ErrorDialog;
import com.blueshield.appsws.dialoges.FullScreenDialog;
import com.blueshield.appsws.models.jobdetails.ClientInfo;
import com.blueshield.appsws.models.jobdetails.EquipmentRequirement;
import com.blueshield.appsws.models.jobdetails.FilesDatum;
import com.blueshield.appsws.models.jobdetails.JobDetail;
import com.blueshield.appsws.models.jobdetails.JobStatusResponse;
import com.blueshield.appsws.models.jobdetails.ProjectInfo;
import com.blueshield.appsws.models.jobdetails.ResponseJobDetail;
import com.blueshield.appsws.models.jobdetails.RosteredStaff;
import com.blueshield.appsws.models.jobdetails.StaffingRequirement;
import com.blueshield.appsws.otto.BusProvider;
import com.blueshield.appsws.otto.JobChangedEvent;
import com.blueshield.appsws.preferences.PreferenceManager;
import com.squareup.otto.Subscribe;

import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.blueshield.appsws.common.Constants.IS_ACCEPTABLE;
import static com.blueshield.appsws.common.Constants.JOB_ID;
import static com.blueshield.appsws.common.Constants.JOB_ONLY_ID;
import static com.blueshield.appsws.common.Constants.RESOURCE_ID;

public class JobDetailActivity extends AppCompatActivity implements View.OnClickListener, FullScreenDialog.RejectButtonListner {

    private static final String APPROVE = "approve";
    private static final String REJECT = "rejected";
    @BindView(R.id.progressBar)
    ProgressBar mProgressView;

    @BindView(R.id.mainLayout)
    ViewGroup mainLayout;

    @BindView(R.id.jobTitle)
    TextView jobTitle;

    @BindView(R.id.shiftTitle)
    TextView shiftTitle;

    @BindView(R.id.workRole)
    TextView workRole;

    @BindView(R.id.startDate)
    TextView startDate;

    @BindView(R.id.startTime)
    TextView startTime;

    @BindView(R.id.endDate)
    TextView endDate;

    @BindView(R.id.endTime)
    TextView endTime;

    @BindView(R.id.siteContact)
    TextView siteContact;

    @BindView(R.id.mobileNumber)
    TextView mobileNumber;

    @BindView(R.id.billableClient)
    TextView billableClient;

    @BindView(R.id.orderNumber)
    TextView orderNumber;

    @BindView(R.id.preStartLocation)
    TextView preStartLocation;

    @BindView(R.id.workSiteLocation)
    TextView workSiteLocation;

    @BindView(R.id.protectionMethod)
    TextView protectionMethod;

    @BindView(R.id.shiftNotes)
    WebView shiftNotes;

    @BindView(R.id.jobDescription)
    WebView jobDescription;

    @BindView(R.id.network)
    TextView network;

    @BindView(R.id.keyProject)
    TextView keyProject;


    @BindView(R.id.fileNames)
    RecyclerView fileNames;

    @BindView(R.id.equipmentRequirement)
    RecyclerView equipmentRequirement;

    @BindView(R.id.rosteredStaff)
    RecyclerView rosteredStaff;

    @BindView(R.id.footerButtons)
    View footerButtons;

    @BindView(R.id.btnAccept)
    Button btnAccept;

    @BindView(R.id.btnReject)
    Button btnReject;

    @BindView(R.id.rostHeading)
    LinearLayout rostHeading;
    private JobDetail jobDetail;

    protected ErrorDialog errorDialog;

    private int jobId;
    private int roasterTableId;
    private boolean isAcceptable;

    EquipmentListAdapter equipmentListAdapter;
    List<EquipmentRequirement> equipmentRequirements = new ArrayList<>();

    FilesListAdapter filesListAdapter;
    List<FilesDatum> filesData = new ArrayList<>();

    RoastedListAdapter roastedListAdapter;

    List<RosteredStaff> rosteredStaffArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_detail);
        setTitle(getString(R.string.shifts));
        ButterKnife.bind(this);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
            actionBar.setDisplayHomeAsUpEnabled(true);
        jobId = getIntent().getIntExtra(JOB_ID, 0);
        roasterTableId = getIntent().getIntExtra(JOB_ONLY_ID, 0);
        int resourceId = getIntent().getIntExtra(RESOURCE_ID, 0);
        isAcceptable = getIntent().getBooleanExtra(IS_ACCEPTABLE, false);
        fetchJobDetail(jobId, resourceId);
        //fetchJobDetail(63,357);
        errorDialog = new ErrorDialog(this, getString(R.string.went_wrong));

        btnAccept.setOnClickListener(this);
        btnReject.setOnClickListener(this);
        filesListAdapter = new FilesListAdapter(filesData, this);
        roastedListAdapter = new RoastedListAdapter(rosteredStaffArrayList);
        equipmentListAdapter = new EquipmentListAdapter(equipmentRequirements);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
        fileNames.setLayoutManager(mLayoutManager);
        fileNames.setAdapter(filesListAdapter);

        //second recycler
        //equipmentRequirement.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager aLayoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
        equipmentRequirement.setLayoutManager(aLayoutManager);
        equipmentRequirement.setAdapter(equipmentListAdapter);

        //third recycler
        RecyclerView.LayoutManager eLayoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
        rosteredStaff.setLayoutManager(eLayoutManager);
        rosteredStaff.setAdapter(roastedListAdapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    private void fetchJobDetail(int jobId, int resId) {
        showProgress(true);
        String id = PreferenceManager.getInstance().getId();
        String user_token = PreferenceManager.getInstance().getToken();
        Call<JobDetail> call = RailApplication.getWebService().getJobDetail(id, user_token, jobId, resId);
        call.enqueue(new Callback<JobDetail>() {
            @Override
            public void onResponse(@NotNull Call<JobDetail> call, @NotNull Response<JobDetail> response) {
                showProgress(false);
                mainLayout.setVisibility(View.VISIBLE);
                jobDetail = response.body();
                fillUpUI();
                if (jobDetail != null && jobDetail.getResponse() != null) {
                    equipmentListAdapter.setEquipmentRequirements(jobDetail.getResponse().getEquipmentRequirements());
                    equipmentListAdapter.notifyDataSetChanged();

                    filesListAdapter.setFilesData(jobDetail.getResponse().getFilesData());
                    filesListAdapter.notifyDataSetChanged();
                    rosteredStaffArrayList = jobDetail.getResponse().getRosteredStaff();
                    if (rosteredStaffArrayList != null && rosteredStaffArrayList.size() > 0) {
                        rostHeading.setVisibility(View.VISIBLE);
                    }
                    roastedListAdapter.setRoastedList(rosteredStaffArrayList);
                    roastedListAdapter.notifyDataSetChanged();
                    if (jobDetail.getResponse().getRosterTableId() != null)
                        roasterTableId = jobDetail.getResponse().getRosterTableId();

                }

            }

            @Override
            public void onFailure(@NotNull Call<JobDetail> call, @NotNull Throwable t) {
                showProgress(false);
                errorDialog.show();
            }
        });
    }

    private void fillUpUI() {

        ResponseJobDetail responseJobDetail = jobDetail.getResponse();
        ProjectInfo projectInfo = responseJobDetail.getProjectInfo();
        ClientInfo clientInfo = responseJobDetail.getClientInfo();
        StaffingRequirement staffingRequirement = null;
        if (responseJobDetail.getStaffingRequirements().size() > 0) {

            staffingRequirement = responseJobDetail.getStaffingRequirements().get(0);
        }

        if (projectInfo != null) {
            if (projectInfo.getProjectName() != null)
                jobTitle.setText(projectInfo.getProjectName());
            if (projectInfo.getLocation() != null) {
                preStartLocation.setText(projectInfo.getLocation());
                preStartLocation.setOnClickListener(this);
            }
            if (projectInfo.getClientContactName() != null)
                siteContact.setText(projectInfo.getClientContactName());
            if (projectInfo.getPhoneNumberOnSite() != null)
                mobileNumber.setText(projectInfo.getPhoneNumberOnSite().toString());

            if (projectInfo.getWorksiteLocation() != null) {
                workSiteLocation.setText(projectInfo.getWorksiteLocation().toString());
            }
            if (projectInfo.getBookingId() != null) {
                orderNumber.setText(projectInfo.getPoNumber());
            }
            if (projectInfo.getRailNetwork() != null) {
                network.setText(projectInfo.getRailNetwork());
            }
            if (projectInfo.getKeyProjectName() != null) {
                keyProject.setText(projectInfo.getKeyProjectName());
            }
            if (projectInfo.getProtectionMethod() != null)
                protectionMethod.setText(projectInfo.getProtectionMethod());
            if (projectInfo.getDescription() != null) {

                jobDescription.getSettings();
                jobDescription.setBackgroundColor(Color.parseColor("#E5E5EA"));
                jobDescription.loadData(projectInfo.getDescription(), "text/html; charset=utf-8", "UTF-8");

            }
            if (projectInfo.getCompanyName() != null) {
                billableClient.setText(projectInfo.getCompanyName().toString());
            }
        }
        if (staffingRequirement != null) {
            if (staffingRequirement.getShiftTitle() != null) {
                shiftTitle.setText(staffingRequirement.getShiftTitle());
            }
            if (staffingRequirement.getResourceCode() != null) {
                workRole.setText(staffingRequirement.getResourceCode());
            }
            if (staffingRequirement.getStartDate() != null) {
                try {
                    startDate.setText(Utils.changeDateFormat(staffingRequirement.getStartDate(), "yyyy-MM-dd", "dd/MM/yyyy"));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (staffingRequirement.getEndDate() != null) {
                try {
                    endDate.setText(Utils.changeDateFormat(staffingRequirement.getEndDate(), "yyyy-MM-dd", "dd/MM/yyyy"));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (staffingRequirement.getStartTime() != null) {
                try {
                    startTime.setText(Utils.changeDateFormat(staffingRequirement.getStartTime(), "HH:mm:ss", "HH:mm"));
                } catch (ParseException e) {
                    startTime.setText(staffingRequirement.getStartTime());
                }
            }
            if (staffingRequirement.getEndTime() != null) {
                try {
                    endTime.setText(Utils.changeDateFormat(staffingRequirement.getEndTime(), "HH:mm:ss", "HH:mm"));
                } catch (ParseException e) {
                    e.printStackTrace();
                    endTime.setText(staffingRequirement.getEndTime());
                }
            }
            if (staffingRequirement.getNotes() != null)
                shiftNotes.loadData(staffingRequirement.getNotes(), "text/html; charset=utf-8", "UTF-8");
        } else {

            RosteredStaff staff;
            if (rosteredStaffArrayList.size() > 0) {
                staff = rosteredStaffArrayList.get(0);
                if (staff.getTitle() != null) {
                    shiftTitle.setText(staff.getTitle());
                }
            }

        }


        if (responseJobDetail.getNewJob() != null) {
            isAcceptable = responseJobDetail.getNewJob();
        }

        if (isAcceptable) {
            footerButtons.setVisibility(View.VISIBLE);
        } else {
            footerButtons.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (!isTaskRoot()) {
                    finish();
                } else {
                    finish();
                    callMainActivity();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
        if (show) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    private void openDialog() {

        FullScreenDialog fullScreenDialog = FullScreenDialog.display(getSupportFragmentManager());
        fullScreenDialog.setListner(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnAccept:
                changeStatus(APPROVE);
                break;
            case R.id.btnReject:
                openDialog();
                break;
            case R.id.preStartLocation:
                ResponseJobDetail responseJobDetail = jobDetail.getResponse();
                ProjectInfo projectInfo = responseJobDetail.getProjectInfo();
                String map = "http://maps.google.co.in/maps?q=" + projectInfo.getLocation();
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
                startActivity(i);
                break;
            default:
                break;
        }
    }

    private void changeStatus(String status) {
        showProgress(true);
        String id = PreferenceManager.getInstance().getId();
        String user_token = PreferenceManager.getInstance().getToken();
        Call<JobStatusResponse> call = RailApplication.getWebService().changeJobStatus(id, user_token, jobId, this.roasterTableId, status);
        call.enqueue(new Callback<JobStatusResponse>() {
            @Override
            public void onResponse(@NotNull Call<JobStatusResponse> call, @NotNull Response<JobStatusResponse> response) {
                showProgress(false);
                JobStatusResponse jobStatusResponse = response.body();
                if (jobStatusResponse != null && jobStatusResponse.getStatus().equals("success")) {
                    Toast.makeText(getApplicationContext(), "status changed", Toast.LENGTH_LONG).show();
                    callMainActivity();
                } else {
                    errorDialog.show();
                }
            }

            @Override
            public void onFailure(Call<JobStatusResponse> call, Throwable t) {
                t.printStackTrace();
                showProgress(false);
                errorDialog.show();
            }
        });

    }

    public void callMainActivity() {
        Intent mainActivityIntent = new Intent(this, MainActivity.class); //Show second fragment
        startActivity(mainActivityIntent);
        finish();
    }

    @Override
    public void onRejectClick(String reason) {
        changeStatus(REJECT, reason);
    }

    private void changeStatus(String status, String reason) {

        showProgress(true);
        String id = PreferenceManager.getInstance().getId();
        String user_token = PreferenceManager.getInstance().getToken();
        Call<JobStatusResponse> call = RailApplication.getWebService().changeJobStatusWithReason(id, user_token, jobId, this.roasterTableId, status, reason);
        call.enqueue(new Callback<JobStatusResponse>() {
            @Override
            public void onResponse(@NotNull Call<JobStatusResponse> call, @NotNull Response<JobStatusResponse> response) {
                showProgress(false);
                JobStatusResponse jobStatusResponse = response.body();
                if (jobStatusResponse != null && jobStatusResponse.getStatus().equals("success")) {
                    Toast.makeText(getApplicationContext(), "status changed", Toast.LENGTH_LONG).show();
                    callMainActivity();
                } else {
                    errorDialog.show();
                }
            }

            @Override
            public void onFailure(Call<JobStatusResponse> call, Throwable t) {
                t.printStackTrace();
                showProgress(false);
                errorDialog.show();
            }
        });

    }

    @Subscribe
    public void onJobChanged(JobChangedEvent event) {
        fetchJobDetail(event.jobId, event.resourceRequirementId);
    }
}
