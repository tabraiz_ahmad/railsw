package com.blueshield.appsws.activities;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.blueshield.appsws.R;
import com.blueshield.appsws.common.Constants;
import com.blueshield.appsws.fragments.AvailabilityFragment;
import com.blueshield.appsws.fragments.MessagesFragment;
import com.blueshield.appsws.fragments.MoreFragment;
import com.blueshield.appsws.fragments.SlidingJobFragment;
import com.blueshield.appsws.otto.BusProvider;
import com.blueshield.appsws.preferences.PreferenceManager;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.messaging.RemoteMessage;
import com.squareup.otto.Subscribe;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import static com.blueshield.appsws.common.Constants.JOB_ID;
import static com.blueshield.appsws.common.Constants.MESSAGE_ID;
import static com.blueshield.appsws.common.Constants.NOTIFICATION_ID;
import static com.blueshield.appsws.common.Constants.RESOURCE_ID;
import static com.blueshield.appsws.services.MyFirebaseMessagingService.channelId;

public class MainActivity extends AppCompatActivity {


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        switch (item.getItemId()) {
            case R.id.navigation_jobs:
                showSlidingJobFragment();
                return true;
            case R.id.navigation_availability:
                showAvailiblityFragment();
                return true;
            case R.id.navigation_messages:
                showMessagesFragment();
                return true;
            case R.id.navigation_more:
                showMoreFragment();
                return true;
        }
        return false;
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navigation = findViewById(R.id.navigation);

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        if (savedInstanceState == null) {
            showSlidingJobFragment();
        }
    }


    private void showSlidingJobFragment() {

        SlidingJobFragment slidingJobFragment = SlidingJobFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, slidingJobFragment, SlidingJobFragment.TAG)
                .addToBackStack(null)
                .commit();
    }

    private void showMessagesFragment() {
        MessagesFragment messagesFragment = MessagesFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, messagesFragment, MessagesFragment.TAG)
                .addToBackStack(null)
                .commit();

    }

    private void showAvailiblityFragment() {
        AvailabilityFragment availabilityFragment = AvailabilityFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, availabilityFragment, AvailabilityFragment.TAG)
                .addToBackStack(null)
                .commit();
    }

    private void showMoreFragment() {
        MoreFragment moreFragment = MoreFragment.newInstance();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.container, moreFragment, MoreFragment.TAG)
                .addToBackStack(null)
                .commit();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.ADD_AVAILIBILTY_REQUEST_CODE) {
            showAvailiblityFragment();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    @Subscribe
    public void onNotification(RemoteMessage remoteMessage) {

        Intent intent = null;
        Log.e("remoteMessage", remoteMessage.toString());
        if (remoteMessage.getData().keySet().contains("message_info")) {
            try {
                JSONObject jsonObject = new JSONObject(remoteMessage.getData().get("message_info"));
                if (jsonObject.has("type") && jsonObject.get("type").equals("job")) {
                    intent = new Intent(this, JobDetailActivity.class);
                    int jobId = Integer.parseInt(String.valueOf(jsonObject.get("project_id")));
                    int resourceId = Integer.parseInt(String.valueOf(jsonObject.get("resource_requirement_id")));
                    intent.putExtra(JOB_ID, jobId);
                    intent.putExtra(RESOURCE_ID, resourceId);
                } else {
                    intent = new Intent(this, MessagesDetailActivity.class);
                    String messageId = jsonObject.get("main_message_id").toString();
                    intent.putExtra(MESSAGE_ID, messageId);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        /*// Create Notification
         */
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_stat_ic_notification)
                .setContentTitle(remoteMessage.getNotification().getTitle())
                .setContentText(remoteMessage.getNotification().getBody())
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "railsws_channel",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
            notificationBuilder.setChannelId(channelId);
        }
        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
    }

}
