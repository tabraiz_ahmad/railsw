package com.blueshield.appsws.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.blueshield.appsws.R;
import com.blueshield.appsws.RailApplication;
import com.blueshield.appsws.common.Utils;
import com.blueshield.appsws.dialoges.ErrorDialog;
import com.blueshield.appsws.dialoges.InfoDialog;
import com.blueshield.appsws.preferences.PreferenceManager;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddUnavailabilityActivity extends AppCompatActivity {

    @BindView(R.id.progressBar)
    ProgressBar mProgressView;

    @BindView(R.id.unavailabilityType)
    Spinner unavailabilityTypeSpinner;

    @BindView(R.id.fromDate)
    TextView fromDateTV;

    @BindView(R.id.fromDateET)
    EditText fromDateET;


    @BindView(R.id.toDate)
    TextView toDateTV;

    @BindView(R.id.toDateET)
    EditText toDateET;

    @BindView(R.id.reasonET)
    EditText reasonET;

    @BindView(R.id.btnAddUnavailability)
    Button btnAddUnavailability;

    protected ErrorDialog errorDialog;
    protected ErrorDialog formErrorDialog;
    protected InfoDialog infoDialog;

    private Map<String, String> dropDownMap;

    private List<String> listValues = new ArrayList<>();

    private ArrayAdapter<String> spinnerAdaper;

    public static String phoneNumber = "";

    Calendar dateCalendar;
    private String selectedId = "";

    String toDate;
    String fromDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_availability);
        setTitle(getString(R.string.add_unavailability));
        ButterKnife.bind(this);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
            actionBar.setDisplayHomeAsUpEnabled(true);

        spinnerAdaper = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, listValues);
        spinnerAdaper.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        unavailabilityTypeSpinner.setAdapter(spinnerAdaper);

        unavailabilityTypeSpinner.setOnItemSelectedListener(unavailbilitySpinnerListner);


        errorDialog = new ErrorDialog(this, getString(R.string.went_wrong));

        formErrorDialog = new ErrorDialog(this, getString(R.string.went_wrong));

        dateCalendar = Calendar.getInstance();

        fetchDropDownData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
        if (show) {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    private void fetchDropDownData() {

        showProgress(true);
        String id = PreferenceManager.getInstance().getId();
        String user_token = PreferenceManager.getInstance().getToken();
        Call<ResponseBody> call = RailApplication.getWebService().getUnavailabilityDropDownData(id, user_token);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                showProgress(false);

                String jsonData;
                try {
                    assert response.body() != null;
                    jsonData = response.body().string();
                    JSONObject Jobject = new JSONObject(jsonData);
                    Log.e("Drop", Jobject.toString());
                    if (Jobject.get("status").equals("success")) {
                        JSONObject jsonResponse = Jobject.getJSONObject("response");
                        dropDownMap = new HashMap<>();
                        dropDownMap = Utils.toMap(jsonResponse);
                        listValues.clear();
                        listValues.add(getString(R.string.choose_unavailability_type));
                        listValues.addAll(Utils.getListOfKeys(dropDownMap));
                        spinnerAdaper.notifyDataSetChanged();
                    } else {
                        showProgress(false);
                        errorDialog.show();
                        listValues.clear();
                        listValues.add(getString(R.string.choose_unavailability_type));
                        spinnerAdaper.notifyDataSetChanged();
                    }

                } catch (IOException | JSONException | NullPointerException e) {
                    e.printStackTrace();
                    showProgress(false);
                    errorDialog.show();
                    listValues.clear();
                    listValues.add(getString(R.string.choose_unavailability_type));
                    spinnerAdaper.notifyDataSetChanged();

                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                showProgress(false);
                errorDialog.show();
            }
        });
    }

    @OnClick(R.id.fromDateET)
    void showStartDateDialog() {
        new DatePickerDialog(AddUnavailabilityActivity.this, onStartDateSetListener, dateCalendar
                .get(Calendar.YEAR), dateCalendar.get(Calendar.MONTH),
                dateCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @OnClick(R.id.toDateET)
    void showEndDateDialog() {
        new DatePickerDialog(AddUnavailabilityActivity.this, onEndDateSetListener, dateCalendar
                .get(Calendar.YEAR), dateCalendar.get(Calendar.MONTH),
                dateCalendar.get(Calendar.DAY_OF_MONTH)).show();
    }

    @OnClick(R.id.btnAddUnavailability)
    void addUnavailability() {

        if (selectedId == null) {
            formErrorDialog.show();
            formErrorDialog.setMessage(getString(R.string.please_choose_unavailbility_type));
            return;
        } else {
            if (fromDateET.getText().length() < 1 || toDateET.getText().length() < 1) {
                formErrorDialog.show();
                formErrorDialog.setMessage(getString(R.string.please_select_to_date));
                return;
            } else if (reasonET.getText().length() < 1) {
                formErrorDialog.show();
                formErrorDialog.setMessage(getString(R.string.please_select_to_reason));
                return;
            }
        }

        showProgress(true);
        String id = PreferenceManager.getInstance().getId();
        String user_token = PreferenceManager.getInstance().getToken();
        Map<String, String> timingMap = new HashMap<>();
        String type;
        timingMap.put("start_date", fromDate);
        timingMap.put("end_date", toDate);
        type = "multiple_days";

        Call<ResponseBody> call = RailApplication.getWebService().addUnavailability(id, user_token, selectedId, type, reasonET.getText().toString(), timingMap);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {

                String jsonData;
                try {
                    showProgress(false);
                    if (response.body() != null) {
                        jsonData = response.body().string();
                        final JSONObject Jobject = new JSONObject(jsonData);
                        if (Jobject.get("status").equals("success")) {
                            Log.d("Added", Jobject.toString());
                            sendResultBack();
                        } else if (Jobject.get("status").equals("pending")) {
                            infoDialog = new InfoDialog(AddUnavailabilityActivity.this, Jobject.get("response").toString(), new InfoDialog.DialogButtonsListner() {
                                @Override
                                public void onPositiveClick() {

                                    infoDialog.dismiss();

                                    try {
                                        phoneNumber = Jobject.get("rosteredNumber").toString();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    Intent intent = new Intent(Intent.ACTION_DIAL);
                                    intent.setData(Uri.parse("tel:" + phoneNumber));
                                    startActivity(intent);

                                }

                                @Override
                                public void onNegativeClick() {
                                    infoDialog.dismiss();
                                }
                            });
                            infoDialog.show();
                            infoDialog.setNegativeButtonText("Cancel Request");
                            infoDialog.setPositiveButtonText("Call Rosters");
                            //sendResultBack();
                        }
                    }
                } catch (IOException | JSONException | NullPointerException e) {
                    e.printStackTrace();
                    showProgress(false);
                    errorDialog.show();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                showProgress(false);
                errorDialog.show();
            }
        });
    }

    private void sendResultBack() {
        Intent resultIntent = new Intent();
        //resultIntent.putExtra("colorcode", colorcode );
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }

    private void updateLabel(boolean isStartDate) {
        String serverFormat = "yyyy-MM-dd HH:mm";
        String myFormat = "dd/MM/YYYY HH:mm";

        SimpleDateFormat sdfServer = new SimpleDateFormat(serverFormat, Locale.US);
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        if (isStartDate) {
            fromDateET.setText(sdf.format(dateCalendar.getTime()));
            fromDate = sdfServer.format(dateCalendar.getTime());
        } else {
            toDate = sdfServer.format(dateCalendar.getTime());
            toDateET.setText(sdf.format(dateCalendar.getTime()));
        }
    }

    DatePickerDialog.OnDateSetListener onStartDateSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            dateCalendar.set(Calendar.YEAR, year);
            dateCalendar.set(Calendar.MONTH, monthOfYear);
            dateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker = new TimePickerDialog(AddUnavailabilityActivity.this, startTimeListner, hour, minute, true);//Yes 24 hour time
            mTimePicker.setTitle("Select Start Time");
            mTimePicker.show();
        }

    };

    TimePickerDialog.OnTimeSetListener startTimeListner = (view, hourOfDay, minute) -> {
        dateCalendar.set(Calendar.HOUR_OF_DAY , hourOfDay);
        dateCalendar.set(Calendar.MINUTE, minute);
        updateLabel(true);
    };

    TimePickerDialog.OnTimeSetListener endTimeListner = (view, hourOfDay, minute) -> {
        dateCalendar.set(Calendar.HOUR_OF_DAY , hourOfDay);
        dateCalendar.set(Calendar.MINUTE, minute);
        updateLabel(false);
    };

    DatePickerDialog.OnDateSetListener onEndDateSetListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            dateCalendar.set(Calendar.YEAR, year);
            dateCalendar.set(Calendar.MONTH, monthOfYear);
            dateCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            Calendar mcurrentTime = Calendar.getInstance();
            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
            int minute = mcurrentTime.get(Calendar.MINUTE);
            TimePickerDialog mTimePicker = new TimePickerDialog(AddUnavailabilityActivity.this, endTimeListner, hour, minute, true);//Yes 24 hour time
            mTimePicker.setTitle("Select End Time");
            mTimePicker.show();
        }

    };

    AdapterView.OnItemSelectedListener unavailbilitySpinnerListner = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (listValues != null) {
                String selectedItemValue = listValues.get(position);
                if (dropDownMap != null)
                    selectedId = dropDownMap.get(selectedItemValue);
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };
}
