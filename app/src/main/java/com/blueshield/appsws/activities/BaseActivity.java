package com.blueshield.appsws.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;

import com.blueshield.appsws.dialoges.ErrorDialog;

/**
 * Created by tabraiz on 20/07/18.
 */

public class BaseActivity extends Activity {
    protected ErrorDialog errorDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void callMainActivity() {
        Intent mainActivityIntent = new Intent(this, MainActivity.class); //Show second fragment
        startActivity(mainActivityIntent);
        finish();
    }
}
