package com.blueshield.appsws.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blueshield.appsws.R;
import com.blueshield.appsws.models.notifications.NotificationsArray;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationListAdapter extends RecyclerView.Adapter<NotificationListAdapter.NotificationViewHolder> implements View.OnClickListener {

    private NotificationListAdapter.NotificationClickListner callbackInterface;


    public interface NotificationClickListner {
        void onNotificationRowClick(NotificationsArray notificationsArray);
    }


    private List<NotificationsArray> notificationsArray;

    public NotificationListAdapter(List<NotificationsArray> notificationsArray, NotificationClickListner notificationClickListner) {
        this.notificationsArray = notificationsArray;
        callbackInterface = notificationClickListner;
    }

    public List<NotificationsArray> getNotificationsArray() {
        return notificationsArray;
    }

    public void setNotificationsArray(List<NotificationsArray> notificationsArray) {
        this.notificationsArray = notificationsArray;
    }

    @NonNull
    @Override
    public NotificationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_notification_list, parent, false);
        return new NotificationViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationViewHolder holder, int position) {

        NotificationsArray notification = notificationsArray.get(position);
        holder.notificationTime.setText(notification.getNotificationTime());
        holder.notificationDetail.setText(notification.getNotificationDetail());
        holder.notificationTitle.setText(notification.getNotificationTitle());
        holder.itemView.setTag(position);
        holder.itemView.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return notificationsArray != null ? notificationsArray.size() : 0;
    }

    @Override
    public void onClick(View v) {
        int position = (int) v.getTag();
        NotificationsArray notification = notificationsArray.get(position);
        callbackInterface.onNotificationRowClick(notification);
    }

    class NotificationViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.notification_title)
        TextView notificationTitle;

        @BindView(R.id.notification_detail)
        TextView notificationDetail;

        @BindView(R.id.notification_time)
        TextView notificationTime;

        NotificationViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
