package com.blueshield.appsws.adapters;

import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import com.blueshield.appsws.R;
import com.blueshield.appsws.drawables.ColorCircleDrawable;
import com.blueshield.appsws.models.messages.MyMessage;
import com.blueshield.appsws.preferences.PreferenceManager;

public class MessagesListAdapter extends RecyclerView.Adapter<MessagesListAdapter.MessageViewHolder> {

    private MessagesListAdapterListner callbackInterface;

    public interface MessagesListAdapterListner {
        void onMessageRowClick(String mainMessageId, String username);
    }

    private List<MyMessage> myMessageList;

    public void setMyMessageList(List<MyMessage> myMessageList) {
        this.myMessageList = myMessageList;
    }

    public MessagesListAdapter(List<MyMessage> myMessageList, MessagesListAdapterListner callbackInterface) {
        this.myMessageList = myMessageList;
        this.callbackInterface = callbackInterface;
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_message_list, parent, false);
        return new MessageViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder holder, int position) {

        MyMessage myMessage = myMessageList.get(position);
        holder.from.setText(myMessage.getUserName());
        holder.time.setText(myMessage.getCreatedAt());
        holder.message.setText(myMessage.getSubject());
        if (!myMessage.getStatus().equals("read")) {
            holder.newMessage.setBackground(new ColorCircleDrawable(Color.RED));
            holder.newMessage.setVisibility(View.VISIBLE);

        } else {
            holder.newMessage.setVisibility(View.INVISIBLE);
        }
        holder.itemView.setTag(myMessage);
    }

    @Override
    public int getItemCount() {
        return myMessageList != null ? myMessageList.size() : 0;
    }


    class MessageViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.from)
        TextView from;


        @BindView(R.id.newMessage)
        TextView newMessage;

        @BindView(R.id.time)
        TextView time;

        @BindView(R.id.message)
        TextView message;

        MessageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    MyMessage myMessage = (MyMessage) v.getTag();
                    int index = myMessageList.indexOf(myMessage);
                    myMessageList.get(index).setStatus("read");
                    notifyDataSetChanged();
                    callbackInterface.onMessageRowClick(myMessage.getMainMessageId().toString(), myMessage.getUserName());
                }
            });
        }
    }


}
