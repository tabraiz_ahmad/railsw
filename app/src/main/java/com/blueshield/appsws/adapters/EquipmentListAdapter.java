package com.blueshield.appsws.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blueshield.appsws.R;
import com.blueshield.appsws.models.jobdetails.EquipmentRequirement;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tabraiz on 30/07/18.
 */

public class EquipmentListAdapter extends RecyclerView.Adapter<EquipmentListAdapter.TextViewHolder>{

    List<EquipmentRequirement> equipmentRequirements;

    public EquipmentListAdapter(List<EquipmentRequirement> equipmentRequirements) {
        this.equipmentRequirements = equipmentRequirements;
    }

    @NonNull
    @Override
    public TextViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_equipment_requirement, parent, false);

        return new EquipmentListAdapter.TextViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TextViewHolder holder, int position) {
        EquipmentRequirement equipmentRequirement = equipmentRequirements.get(position);
        if(equipmentRequirement != null){
            holder.equipmentDetails.setText(equipmentRequirement.getHardware());
            holder.equipmentQuantity.setText(equipmentRequirement.getQuantity().toString());
        }
    }

    @Override
    public int getItemCount() {
        return equipmentRequirements==null ? 0 : equipmentRequirements.size();
    }

    public void setEquipmentRequirements(List<EquipmentRequirement> equipmentRequirements) {
        this.equipmentRequirements = equipmentRequirements;
    }

    public class TextViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.equipmentDetails)
        TextView equipmentDetails;

        @BindView(R.id.equipmentQuantity)
        TextView equipmentQuantity;

        public TextViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
