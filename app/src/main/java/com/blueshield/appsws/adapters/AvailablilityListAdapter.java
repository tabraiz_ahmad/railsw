package com.blueshield.appsws.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.blueshield.appsws.R;
import com.blueshield.appsws.models.availability.AvailabilityResponse;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AvailablilityListAdapter extends RecyclerView.Adapter<AvailablilityListAdapter.AvailabilityViewHolder> {

    private List<AvailabilityResponse> availabilityResponseList = new ArrayList<>();
    private AvailablilityListInterface callbackInterface;


    // AvailablilityListAdapter Interface

    public interface AvailablilityListInterface {
        void onRowClick(Integer id);
    }
    public void setAvailabilityResponseList(List<AvailabilityResponse> availabilityResponseList) {
        this.availabilityResponseList = availabilityResponseList;
    }

    public AvailablilityListAdapter(List<AvailabilityResponse> availabilityResponseList, AvailablilityListInterface callbackInterface) {
        this.availabilityResponseList = availabilityResponseList;
        this.callbackInterface = callbackInterface;
    }

    @NonNull
    @Override
    public AvailabilityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_unavailability_list, parent, false);
        return new AvailabilityViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AvailabilityViewHolder holder, int position) {
        AvailabilityResponse availabilityResponse = availabilityResponseList.get(position);
        holder.date.setText(availabilityResponse.getStartDate());
        holder.duration.setText(availabilityResponse.getDuration());
        holder.unavailabilityTitle.setText(availabilityResponse.getLeaveTypeTitle());
        holder.deleteBtn.setTag(availabilityResponse.getId());

    }

    @Override
    public int getItemCount() {
        return availabilityResponseList != null ? availabilityResponseList.size() : 0;
    }

    public class AvailabilityViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.date)
        TextView date;

        @BindView(R.id.duration)
        TextView duration;

        @BindView(R.id.unavailabilityTitle)
        TextView unavailabilityTitle;

        @BindView(R.id.deleteBtn)
        ImageButton deleteBtn;

        public AvailabilityViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            deleteBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Integer id = (Integer) v.getTag();
                    callbackInterface.onRowClick(id);
                }
            });
        }
    }
}
