package com.blueshield.appsws.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.blueshield.appsws.R;
import com.blueshield.appsws.models.contacts.Contact;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import jp.wasabeef.picasso.transformations.CropCircleTransformation;

public class ContactsListAdapter extends RecyclerView.Adapter<ContactsListAdapter.ContactViewHolder> implements Filterable, View.OnClickListener {


    private ContactClickListner callbackInterface;
    private boolean checkBox;


    public interface ContactClickListner {
        void onContactRowClick(Contact contact);
    }

    private Filter contactsFilter = new Filter() {
        @Override
        protected Filter.FilterResults performFiltering(CharSequence charSequence) {
            String charString = charSequence.toString();
            Filter.FilterResults filterResults = new FilterResults();
            if (!charString.isEmpty()) {
                List<Contact> filteredList = new ArrayList<>();
                for (Contact row : contactsArray) {
                    if (
                            row.getFirstName().toLowerCase().contains(charString.toLowerCase())
                            || row.getLastName().toLowerCase().contains(charString.toLowerCase())
                            || (row.getPreferredName() !=null && row.getPreferredName().toLowerCase().contains(charString.toLowerCase()))
                            || (row.getPhone() != null && row.getPhone().contains(charSequence))
                    ) {

                        filteredList.add(row);
                    }
                }
                filterResults.values = filteredList;
                filterResults.count = filteredList.size();
            } else {
                filterResults.values = contactsArray;
                filterResults.count = contactsArray.size();
            }
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence charSequence, Filter.FilterResults
                filterResults) {

                contactsFilterArray = (List<Contact>) filterResults.values;
                notifyDataSetChanged();
        }
    };

    public void setContactsArray(List<Contact> contactsArray) {
        this.contactsArray = contactsArray;
        contactsFilterArray = contactsArray;
    }

    public ContactsListAdapter(List<Contact> contactsArray, ContactClickListner listner, boolean checkBox) {
        this.contactsArray = contactsArray;
        callbackInterface = listner;
        this.checkBox = checkBox;
    }

    private List<Contact> contactsArray;
    private List<Contact> contactsFilterArray;


    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_contact_list, parent, false);
        itemView.setOnClickListener(this);
        ContactViewHolder contactViewHolder = new ContactViewHolder(itemView);
        contactViewHolder.contactCheckBox.setOnClickListener(this);
        return contactViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {
        Contact contact = contactsFilterArray.get(position);
        holder.pName.setText(contact.getPreferredName());
        holder.sName.setText(contact.getLastName());
        holder.mobileNumber.setText(contact.getPhone());
        holder.itemView.setTag(contact);
        holder.contactCheckBox.setTag(contact);
        if (this.checkBox) {
            holder.contactCheckBox.setVisibility(View.VISIBLE);
            holder.contactCheckBox.setChecked(contact.isChecked());

        }
        try{
            Log.d("image_path",contact.getImage());
//            PROFIEL_IMAGE_BASE_URL +
                    Picasso.get()
                    .load(contact.getImage())
                    .placeholder(R.drawable.placeholder_profile)
                    .transform(new CropCircleTransformation())
                    .into(holder.profileImage);
        }catch (IllegalArgumentException e){
            Log.e("picasso",e.getMessage());
        }catch (NullPointerException e){
            Log.e("picasso",e.getMessage());

        }


    }

    @Override
    public int getItemCount() {
        return contactsFilterArray != null ? contactsFilterArray.size() : 0;
    }

    @Override
    public Filter getFilter() {
        return contactsFilter;
    }

    @Override
    public void onClick(View v) {
        Contact contact = (Contact) v.getTag();
        callbackInterface.onContactRowClick(contact);

    }

    class ContactViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.pName)
        TextView pName;

        @BindView(R.id.sName)
        TextView sName;

        @BindView(R.id.mobileNumber)
        TextView mobileNumber;

        @BindView(R.id.profileImage)
        ImageView profileImage;

        @BindView(R.id.contactCheckBox)
        CheckBox contactCheckBox;

        ContactViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
