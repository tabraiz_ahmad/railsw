package com.blueshield.appsws.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blueshield.appsws.R;
import com.blueshield.appsws.models.jobdetails.StaffingRequirement;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tabraiz on 30/07/18.
 */

public class StaffListAdapter extends RecyclerView.Adapter<StaffListAdapter.TextViewHolder>{

    public void setStaffingRequirements(List<StaffingRequirement> staffingRequirements) {
        this.staffingRequirements = staffingRequirements;
    }

    List<StaffingRequirement> staffingRequirements = new ArrayList<>();

    @NonNull
    @Override
    public TextViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_staff, parent, false);

        return new TextViewHolder(itemView);
    }

    public StaffListAdapter(List<StaffingRequirement> staffingRequirements) {
        this.staffingRequirements = staffingRequirements;
    }

    @Override
    public void onBindViewHolder(@NonNull TextViewHolder holder, int position) {
        StaffingRequirement staffingRequirement = staffingRequirements.get(position);
        holder.staffDetails.setText(staffingRequirement.getResourceCode());
    }

    @Override
    public int getItemCount() {
        return staffingRequirements == null ? 0 : staffingRequirements.size();
    }

    public class TextViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.staffDetails)
        TextView staffDetails;

        public TextViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
