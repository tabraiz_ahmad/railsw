package com.blueshield.appsws.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blueshield.appsws.R;
import com.blueshield.appsws.models.jobdetails.RosteredStaff;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RoastedListAdapter extends RecyclerView.Adapter<RoastedListAdapter.TextViewHolder>{

    List<RosteredStaff> roastedList;

    public void setRoastedList(List<RosteredStaff> roastedList) {
        this.roastedList = roastedList;
    }

    @NonNull
    @Override
    public TextViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_rost, parent, false);

        return new TextViewHolder(itemView);
    }

    public RoastedListAdapter(List<RosteredStaff> roastedList) {
        this.roastedList = roastedList;
    }

    @Override
    public void onBindViewHolder(@NonNull TextViewHolder holder, int position) {
        RosteredStaff rosteredStaff= roastedList.get(position);
        holder.fullName.setText(String.format("%s %s", rosteredStaff.getPreferredName(), rosteredStaff.getLastName()));
        holder.role.setText(rosteredStaff.getRole());
        holder.date.setText(rosteredStaff.getDate());
        holder.shift.setText(rosteredStaff.getTime());
        holder.title.setText(rosteredStaff.getTitle());
    }

    @Override
    public int getItemCount() {
        return roastedList == null ? 0 : roastedList.size();
    }

    class TextViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.fullName)
        TextView fullName;

        @BindView(R.id.role)
        TextView role;

        @BindView(R.id.date)
        TextView date;

        @BindView(R.id.shift)
        TextView shift;

        @BindView(R.id.title)
        TextView title;

        TextViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
