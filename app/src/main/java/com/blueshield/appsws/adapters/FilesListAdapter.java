package com.blueshield.appsws.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blueshield.appsws.R;
import com.blueshield.appsws.models.jobdetails.FilesDatum;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by tabraiz on 30/07/18.
 */

public class FilesListAdapter extends RecyclerView.Adapter<FilesListAdapter.TextViewHolder>{

    public void setFilesData(List<FilesDatum> filesData) {
        this.filesData = filesData;
    }

    private List<FilesDatum> filesData;
    private Context mContext;

    public FilesListAdapter(List<FilesDatum> filesData, Context mContext) {
        this.filesData = filesData;
        this.mContext = mContext;
    }

    @NonNull

    @Override
    public TextViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_files, parent, false);

        return new FilesListAdapter.TextViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TextViewHolder holder, int position) {
        FilesDatum filesDatum = filesData.get(position);
        if(filesDatum != null){


            /*if(filesDatum.getFileName() != null && filesDatum.getFileName().indexOf("-")>0){
                filesDatum.setFileName(filesDatum.getFileName().substring( filesDatum.getFileName().indexOf("-") +1));
            }*/
            String html = " <a href=\"" + filesDatum.getFileUrl() + "\">" + filesDatum.getFileName() + "</a>";
            Spanned result;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
            } else {
                result = Html.fromHtml(html);
            }

            holder.fileName.setText(result);
            holder.fileName.setMovementMethod(LinkMovementMethod.getInstance());

        }
    }

    @Override
    public int getItemCount() {
        return filesData == null ? 0 : filesData.size();
    }


    class TextViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.fileName)
        TextView fileName;

        TextViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
