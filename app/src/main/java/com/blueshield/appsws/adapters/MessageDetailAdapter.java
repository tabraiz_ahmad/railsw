package com.blueshield.appsws.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blueshield.appsws.R;
import com.blueshield.appsws.models.messages.MessageDetailResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MessageDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_MESSAGE_SENT = 1;
    private static final int VIEW_TYPE_MESSAGE_RECEIVED = 2;

    private List<MessageDetailResponse> mMessageList;

    public MessageDetailAdapter(List<MessageDetailResponse> mMessageList) {
        this.mMessageList = mMessageList;
    }

    public void setMessageList(List<MessageDetailResponse> mMessageList) {
        this.mMessageList = mMessageList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;

        if (viewType == VIEW_TYPE_MESSAGE_SENT) {
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_sent, parent, false);
            return new SentMessageHolder(view);
        } else{
            view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_message_received, parent, false);
            return new ReceivedMessageHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        MessageDetailResponse message = mMessageList.get(position);

        switch (holder.getItemViewType()) {
            case VIEW_TYPE_MESSAGE_SENT:
                ((SentMessageHolder) holder).bind(message);
                break;
            case VIEW_TYPE_MESSAGE_RECEIVED:
                ((ReceivedMessageHolder) holder).bind(message);

        }
    }
    @Override
    public int getItemCount () {
        return mMessageList != null ? mMessageList.size() : 0;
    }

    // Determines the appropriate ViewType according to the sender of the message.
    @Override
    public int getItemViewType ( int position){
        MessageDetailResponse message = mMessageList.get(position);

        if (message.getIsMyMessage().equals("yes")) {
            // If the current user is the sender of the message
            return VIEW_TYPE_MESSAGE_SENT;
        } else {
            // If some other user sent the message
            return VIEW_TYPE_MESSAGE_RECEIVED;
        }
    }

    class ReceivedMessageHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_message_body)
        TextView messageText;

        @BindView(R.id.text_message_time)
        TextView timeText;

        @BindView(R.id.attachment)
        TextView attachment;

        ReceivedMessageHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
        void bind(MessageDetailResponse message ){
            messageText.setText(message.getMessage());
            timeText.setText(message.getMessageTime());
            String path = message.getAttachmentPath();
            if( path != null && path.length() > 0) {
                String html = " <a href=\""+ path +"\">Download Attachment</a>";
                Spanned result;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
                } else {
                    result = Html.fromHtml(html);
                }
                attachment.setText(result);
                attachment.setMovementMethod(LinkMovementMethod.getInstance());
            }else{
                attachment.setVisibility(View.GONE);
            }
        }
    }

    class SentMessageHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_message_body)
        TextView messageText;

        @BindView(R.id.text_message_time)
        TextView timeText;

        @BindView(R.id.attachment)
        TextView attachment;

        SentMessageHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
        void bind(MessageDetailResponse message ){
            messageText.setText(message.getMessage());
            timeText.setText(message.getMessageTime());
            String path = message.getAttachmentPath();
            if( path != null && path.length() > 0){
                String html = " <a href=\""+ path +"\">Download Attachment</a>";
                Spanned result;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    result = Html.fromHtml(html,Html.FROM_HTML_MODE_LEGACY);
                } else {
                    result = Html.fromHtml(html);
                }
                attachment.setText(result);
                attachment.setMovementMethod(LinkMovementMethod.getInstance());
            }else{
                attachment.setVisibility(View.GONE);
            }

        }
    }

}

