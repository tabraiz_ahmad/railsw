package com.blueshield.appsws.adapters;

import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blueshield.appsws.R;
import com.blueshield.appsws.common.Utils;
import com.blueshield.appsws.drawables.ColorCircleDrawable;
import com.blueshield.appsws.models.joblists.Job;
import com.blueshield.appsws.models.joblists.NewJobsApproval;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class JobsListAdapter extends RecyclerView.Adapter<JobsListAdapter.JobViewHolder> {

    // JobsListAdapter Interface

    public interface JobsListAdapterInterface {
        void onRowClick(Integer jobId, Integer id, Integer resourceId, Boolean isAcceptable);
    }

    private JobsListAdapterInterface callbackInterface;
    private List<NewJobsApproval> newJobsApprovalList;
    private List<Job> allJobs;

    public List<NewJobsApproval> getNewJobsApprovalList() {
        return newJobsApprovalList;
    }

    public List<Job> getAllJobs() {
        return allJobs;
    }

    public void setNewJobsApprovalList(List<NewJobsApproval> newJobsApprovalList, int position) {
        this.newJobsApprovalList = new ArrayList<>();
        Calendar now = Calendar.getInstance();
        now.set(Calendar.HOUR, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.MILLISECOND,0);
        long currentTimeMillis = now.getTimeInMillis();
        if (position == 0) {
            for (NewJobsApproval job : newJobsApprovalList) {
                try {
                    long shiftTime = Utils.getDDMMYYYYDate(job.getEndDate());
                    if (shiftTime >= currentTimeMillis) {
                        this.newJobsApprovalList.add(job);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        } else {
            for (NewJobsApproval job : newJobsApprovalList) {
                try {
                    long shiftTime = Utils.getDDMMYYYYDate(job.getEndDate());
                    if (shiftTime < currentTimeMillis) {
                        this.newJobsApprovalList.add(job);
                    }
                } catch (ParseException e) {

                }
            }
        }

    }

    public void setAllJobs(List<Job> allJobs, int position) {
        Calendar now = Calendar.getInstance();
        now.set(Calendar.HOUR_OF_DAY, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.MILLISECOND,0);
        long currentTimeMillis = now.getTimeInMillis();
        this.allJobs = new ArrayList<>();
        if (position == 0) {
            for (Job job : allJobs) {
                try {
                    long shiftTime = Utils.getDDMMYYYYDate(job.getEndDate());
                    if (shiftTime >= currentTimeMillis) {
                        this.allJobs.add(job);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        } else {
            for (Job job : allJobs) {
                try {
                    long shiftTime = Utils.getDDMMYYYYDate(job.getEndDate());
                    if (shiftTime < currentTimeMillis) {
                        this.allJobs.add(job);
                    }
                } catch (ParseException e) {
                }
            }
        }
    }

    public JobsListAdapter(JobsListAdapterInterface jobsListAdapterInterface) {
        callbackInterface = jobsListAdapterInterface;
    }

    @NonNull
    @Override
    public JobViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_job_list, parent, false);
        return new JobViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(@NonNull JobViewHolder holder, int position) {
        if (position < newJobsApprovalList.size()) {
            NewJobsApproval newJobsApproval = newJobsApprovalList.get(position);
            holder.jobTitle.setText(newJobsApproval.getProjectName());
            holder.shiftTitle.setText(newJobsApproval.getShiftTitle());
            holder.clientName.setText(newJobsApproval.getCompanyName());
            holder.preStartLocation.setText(newJobsApproval.getLocation());

            holder.startDate.setText("Start Date: " + newJobsApproval.getStartDate());
            holder.startTime.setText("Start Time: " + newJobsApproval.getStartTime());

            if (newJobsApproval.getDescription() != null && newJobsApproval.getDescription().length() > 0)
                holder.description.setText(Html.fromHtml(newJobsApproval.getDescription()));
            holder.newJobButton.setBackground(new ColorCircleDrawable(Color.RED));
            holder.itemView.setTag(newJobsApproval.getJobId() + "," + newJobsApproval.getId() + "," + newJobsApproval.getResourceRequirementId() + "," + true);
           /* if (newJobsApproval.getProtectionMethod() != null || newJobsApproval.getKeyProjectName() != null) {
                holder.projectKeyInfo.setVisibility(View.VISIBLE);
                holder.protectionMethod.setText(newJobsApproval.getProtectionMethod());
                holder.keyProjectName.setText(newJobsApproval.getKeyProjectName());
            }*/
        } else {
            int otherListPosition = position - newJobsApprovalList.size();
            Job job = allJobs.get(otherListPosition);
            if (job.getCompanyName() != null)
                holder.clientName.setText(job.getCompanyName().toString());
            holder.jobTitle.setText(job.getProjectName());
            holder.shiftTitle.setText(job.getShiftTitle());
            holder.preStartLocation.setText(job.getLocation());
            holder.startDate.setText("Start Date:" + job.getStartDate());
            holder.startTime.setText("Start Time:" + job.getStartTime());
            if (job.getDescription() != null && job.getDescription().length() > 0)
                holder.description.setText(Html.fromHtml(job.getDescription()));
            holder.newJobButton.setVisibility(View.GONE);
            holder.itemView.setTag(job.getJobId() + "," + job.getId()+"," + job.getResourceRequirementId() + "," + false);
//            holder.projectKeyInfo.setVisibility(View.GONE);

        }
    }

    @Override
    public int getItemCount() {
        int newJobSize = newJobsApprovalList == null ? 0 : newJobsApprovalList.size();
        int allJobSize = allJobs == null ? 0 : allJobs.size();
        return newJobSize + allJobSize;
    }


    class JobViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.jobTitle)
        TextView jobTitle;

        @BindView(R.id.shiftTitle)
        TextView shiftTitle;

        @BindView(R.id.clientName)
        TextView clientName;

        @BindView(R.id.startDate)
        TextView startDate;

        @BindView(R.id.preStartLocation)
        TextView preStartLocation;

        @BindView(R.id.startTime)
        TextView startTime;

        @BindView(R.id.description)
        TextView description;

        @BindView(R.id.newJobButton)
        TextView newJobButton;

        /*@BindView(R.id.projectKeyInfo)
        LinearLayout projectKeyInfo;

        @BindView(R.id.projectionMethod)
        TextView protectionMethod;

        @BindView(R.id.keyProjectName)
        TextView keyProjectName;
*/
        JobViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> {
                String jobTag = (String) v.getTag();
                String[] tags = jobTag.split(",");
                String sJobId = tags[0];
                String sId = tags[1];
                String rId = tags[2];

                int id,jobId,requirementId;
                try {
                    jobId = Integer.parseInt(sJobId);
                } catch (NumberFormatException e) {
                    jobId = -1;
                }
                try {
                    id = Integer.parseInt(sId);
                } catch (NumberFormatException e) {
                    id = -1;
                }
                try {
                    requirementId = Integer.parseInt(rId);
                } catch (NumberFormatException e) {
                    requirementId = -1;
                }

                Boolean isAcceptable = Boolean.parseBoolean(tags[3]);
                callbackInterface.onRowClick(jobId, id,requirementId, isAcceptable);
            });
        }
    }
}
