package com.blueshield.appsws.fragments;


import androidx.fragment.app.Fragment;
import android.view.View;
import android.widget.ProgressBar;

import butterknife.Unbinder;
import com.blueshield.appsws.dialoges.ErrorDialog;

/**
 * Created by tabraiz on 13/07/18.
 */

public class BaseFragment extends Fragment {

    protected Unbinder unbinder = null;
    ProgressBar mProgressView;
    protected ErrorDialog errorDialog;

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (unbinder != null) {
            unbinder.unbind();
        }
    }

    protected void showProgressBar(boolean show) {
        if(mProgressView != null){
            mProgressView.setVisibility(show ? View.VISIBLE: View.GONE);
        }
    }

}
