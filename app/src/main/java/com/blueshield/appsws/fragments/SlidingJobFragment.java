package com.blueshield.appsws.fragments;

import android.os.Bundle;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blueshield.appsws.R;
import com.blueshield.appsws.adapters.JobFragmentPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SlidingJobFragment extends BaseFragment {
    public static final String TAG = "SlidingJobFragment ";
    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.sliding_tabs)
    TabLayout slidingTabs;

    public SlidingJobFragment() {
        // Required empty public constructor
    }

    public static SlidingJobFragment newInstance() {
        SlidingJobFragment fragment = new SlidingJobFragment();
        return fragment;
    }

    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(getString(R.string.shifts_cap));

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sliding_job, container, false);;
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view,savedInstanceState);
        viewPager.setAdapter(new JobFragmentPagerAdapter(getActivity().getSupportFragmentManager(),getActivity()));
        slidingTabs.setupWithViewPager(viewPager);

    }
}
