package com.blueshield.appsws.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.blueshield.appsws.R;
import com.blueshield.appsws.RailApplication;
import com.blueshield.appsws.activities.CalanderActivity;
import com.blueshield.appsws.activities.ContactsListActivity;
import com.blueshield.appsws.activities.LoginActivity;
import com.blueshield.appsws.activities.NotificationListActivity;
import com.blueshield.appsws.dialoges.ErrorDialog;
import com.blueshield.appsws.preferences.PreferenceManager;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MoreFragment extends BaseFragment {

    public static final String TAG = "MoreFragment";

    @BindView(R.id.notifications)
    TextView notifications;
    @BindView(R.id.calendar)
    TextView calendar;
    @BindView(R.id.contacts)
    TextView contacts;
    @BindView(R.id.logout)
    TextView logout;

    public MoreFragment() {
        // Required empty public constructor
    }

    public static MoreFragment newInstance() {
        return new MoreFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(getString(R.string.title_more));
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_more, container, false);
        unbinder = ButterKnife.bind(this, view);
        errorDialog = new ErrorDialog(getActivity(),getString(R.string.something_went_wrong));
        return view;
    }

    @OnClick(R.id.logout)
    void onLogoutClick(){
        PreferenceManager.getInstance().clear();
        String id = PreferenceManager.getInstance().getId();
        String user_token = PreferenceManager.getInstance().getToken();
        Call<ResponseBody> call = RailApplication.getWebService().logout(id, user_token);
        moveToLoginScreen();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    @OnClick(R.id.notifications)
    void showNotificationActivity(){
        Intent intent = new Intent(getActivity(), NotificationListActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.calendar)
    void showCalendarActivity(){
        Intent intent = new Intent(getActivity(), CalanderActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.contacts)
    void showContactsActivity(){
        Intent intent = new Intent(getActivity(), ContactsListActivity.class);
        startActivity(intent);
    }


    private void moveToLoginScreen() {
        if(isAdded() && isVisible() && getActivity() != null){
            Intent loginScreen = new Intent(getActivity(), LoginActivity.class);
            (getActivity()).finish();
            loginScreen.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(loginScreen);
        }

    }
}
