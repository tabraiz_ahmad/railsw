package com.blueshield.appsws.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import com.blueshield.appsws.R;
import com.blueshield.appsws.RailApplication;
import com.blueshield.appsws.activities.JobDetailActivity;
import com.blueshield.appsws.adapters.JobsListAdapter;
import com.blueshield.appsws.decorators.DividerItemDecoration;
import com.blueshield.appsws.dialoges.ErrorDialog;
import com.blueshield.appsws.models.joblists.Job;
import com.blueshield.appsws.models.joblists.JobListResponse;
import com.blueshield.appsws.models.joblists.NewJobsApproval;
import com.blueshield.appsws.otto.BusProvider;
import com.blueshield.appsws.otto.JobChangedEvent;
import com.blueshield.appsws.preferences.PreferenceManager;
import com.squareup.otto.Subscribe;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.blueshield.appsws.common.Constants.IS_ACCEPTABLE;
import static com.blueshield.appsws.common.Constants.JOB_ID;
import static com.blueshield.appsws.common.Constants.JOB_ONLY_ID;
import static com.blueshield.appsws.common.Constants.RESOURCE_ID;
import static com.blueshield.appsws.common.Constants.SUCCESS;

public class JobListFragment extends BaseFragment implements JobsListAdapter.JobsListAdapterInterface{

    public static final String TAG = "JobListFragment";
    private static final String POSITION = "position";
    @BindView(R.id.jobsList)
    RecyclerView jobsList;

    @BindView(R.id.emptyTextView)
    TextView emptyTextView;

    private JobListResponse jobListResponse;
    private JobsListAdapter newJobsListAdapter;
    private  List<NewJobsApproval> newJobsApprovals = new ArrayList<>();
    private  List<Job> newJobs = new ArrayList<>();
    private int mPosition;

    public JobListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mPosition = getArguments().getInt(POSITION);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        BusProvider.getInstance().unregister(this);
    }

    public static JobListFragment newInstance(int position) {
        JobListFragment fragment = new JobListFragment();
        Bundle args = new Bundle();
        args.putInt(POSITION,position);
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_job_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        mProgressView = view.findViewById(R.id.progressBar);
        fetchJobsList();
        errorDialog = new ErrorDialog(getActivity(),getString(R.string.something_went_wrong));
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view,savedInstanceState);
        newJobsListAdapter = new JobsListAdapter(this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        jobsList.setLayoutManager(mLayoutManager);
        jobsList.addItemDecoration(new DividerItemDecoration(jobsList.getContext()));
        jobsList.setAdapter(newJobsListAdapter);
    }

    private void fetchJobsList() {
        showProgressBar(true);
        String id = PreferenceManager.getInstance().getId();
        String user_token = PreferenceManager.getInstance().getToken();
        Call<JobListResponse> call = RailApplication.getWebService().getAllJobs(id, user_token);
        call.enqueue(new Callback<JobListResponse>() {
            @Override
            public void onResponse(Call<JobListResponse> call, Response<JobListResponse> response) {
                showProgressBar(false);
                jobListResponse = response.body();
                if(jobListResponse != null && jobListResponse.getStatus().equals(SUCCESS)){
                    newJobsApprovals = jobListResponse.getResponse().getNewJobsApproval();
                    newJobs = jobListResponse.getResponse().getAllJobs();
                    Collections.sort(newJobsApprovals);

                    Collections.sort(newJobs);
                    newJobsListAdapter.setAllJobs(newJobs,mPosition);

                    newJobsListAdapter.setNewJobsApprovalList(newJobsApprovals,mPosition);
                    newJobsListAdapter.notifyDataSetChanged();
                    if(newJobsListAdapter.getAllJobs().size()==0 && newJobsListAdapter.getNewJobsApprovalList().size() ==0){
                        emptyTextView.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<JobListResponse> call, Throwable t) {
                t.printStackTrace();
                Log.e("Error",t.getMessage());
                showProgressBar(false);
                errorDialog.show();
            }
        });
    }

    @Override
    public void onRowClick( Integer jobId, Integer id, Integer resourceId, Boolean isAcceptable) {
        Intent intent = new Intent(getActivity(), JobDetailActivity.class);
        intent.putExtra(JOB_ID, jobId);
        intent.putExtra(JOB_ONLY_ID, id);
        intent.putExtra(IS_ACCEPTABLE,isAcceptable);
        intent.putExtra(RESOURCE_ID,resourceId);
        startActivity(intent);
    }

    @Subscribe
    public void onJobChanged(JobChangedEvent event) {
        fetchJobsList();
    }

}
