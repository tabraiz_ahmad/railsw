package com.blueshield.appsws.fragments;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blueshield.appsws.R;
import com.blueshield.appsws.RailApplication;
import com.blueshield.appsws.activities.ContactsListActivity;
import com.blueshield.appsws.activities.MessagesDetailActivity;
import com.blueshield.appsws.adapters.MessagesListAdapter;
import com.blueshield.appsws.decorators.DividerItemDecoration;
import com.blueshield.appsws.dialoges.ErrorDialog;
import com.blueshield.appsws.models.messages.MyMessage;
import com.blueshield.appsws.models.messages.MyMessagesResponse;
import com.blueshield.appsws.preferences.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.blueshield.appsws.activities.ContactsListActivity.USER_ID_ARRAY_KEY;
import static com.blueshield.appsws.common.Constants.MESSAGE_ID;
import static com.blueshield.appsws.common.Constants.USER_NAME;
import static com.blueshield.appsws.common.Constants.USER_NAME_ARRAY;

public class MessagesFragment extends BaseFragment implements MessagesListAdapter.MessagesListAdapterListner{

    public static final String TAG = "MessagesFragment";
    private static final int CONTACT_REQUEST = 111;

    @BindView(R.id.messagesList)
    RecyclerView messagesList;

    @BindView(R.id.emptyTextView)
    TextView emptyTextView;

    @BindView(R.id.createMessage)
    FloatingActionButton createMessage;

    MessagesListAdapter messagesListAdapter;

    List<MyMessage> myMessageList = new ArrayList<>();

    public MessagesFragment() {
        // Required empty public constructor
    }

    public static MessagesFragment newInstance() {
        MessagesFragment fragment = new MessagesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(getString(R.string.messages));

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        messagesListAdapter = new MessagesListAdapter(myMessageList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        messagesList.setLayoutManager(mLayoutManager);
        messagesList.addItemDecoration(new DividerItemDecoration(messagesList.getContext()));
        messagesList.setAdapter(messagesListAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_messages, container, false);
        unbinder = ButterKnife.bind(this, view);
        mProgressView = view.findViewById(R.id.progressBar);
        fetchMessagesList();
        errorDialog = new ErrorDialog(getActivity(),getString(R.string.something_went_wrong));
        return view;

    }

    private void fetchMessagesList() {

        showProgressBar(true);
        String id = PreferenceManager.getInstance().getId();
        String user_token = PreferenceManager.getInstance().getToken();
        Call<MyMessagesResponse> call = RailApplication.getWebService().getMyMessages(id, user_token);
        call.enqueue(new Callback<MyMessagesResponse>() {
            @Override
            public void onResponse(Call<MyMessagesResponse> call, Response<MyMessagesResponse> response) {
                showProgressBar(false);
                MyMessagesResponse myMessagesResponse = response.body();
                myMessageList = myMessagesResponse.getResponse();
                //Collections.sort(myMessageList);
                messagesListAdapter.setMyMessageList(myMessageList);
                messagesListAdapter.notifyDataSetChanged();
                if(myMessageList == null || myMessageList.size()==0 ){
                    emptyTextView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<MyMessagesResponse> call, Throwable t) {
                showProgressBar(false);
                errorDialog.show();
            }
        });
    }


    @Override
    public void onMessageRowClick(String mainMessageId,String username) {
        Intent intent = new Intent(getActivity(), MessagesDetailActivity.class);
        intent.putExtra(MESSAGE_ID, mainMessageId);
        intent.putExtra(USER_NAME,username);
        startActivity(intent);
    }

    @OnClick(R.id.createMessage)
    void showContactsActivity(){
        Intent intent = new Intent(getActivity(), ContactsListActivity.class);
        startActivityForResult(intent,CONTACT_REQUEST);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CONTACT_REQUEST && resultCode == RESULT_OK){
            ArrayList<Integer> userIdArray = data.getIntegerArrayListExtra(USER_ID_ARRAY_KEY);
            ArrayList<String> userNameArray = data.getStringArrayListExtra(USER_NAME_ARRAY);
            Intent intent = new Intent(getActivity(), MessagesDetailActivity.class);
            intent.putIntegerArrayListExtra(USER_ID_ARRAY_KEY , userIdArray);
            intent.putStringArrayListExtra(USER_NAME_ARRAY,userNameArray);
            startActivity(intent);
        }
    }
}
