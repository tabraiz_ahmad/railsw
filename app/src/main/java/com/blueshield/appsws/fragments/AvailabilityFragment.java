package com.blueshield.appsws.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.blueshield.appsws.R;
import com.blueshield.appsws.RailApplication;
import com.blueshield.appsws.activities.AddUnavailabilityActivity;
import com.blueshield.appsws.adapters.AvailablilityListAdapter;
import com.blueshield.appsws.common.Constants;
import com.blueshield.appsws.decorators.DividerItemDecoration;
import com.blueshield.appsws.dialoges.ErrorDialog;
import com.blueshield.appsws.models.BaseResponse;
import com.blueshield.appsws.models.availability.AvailabilityResponse;
import com.blueshield.appsws.preferences.PreferenceManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AvailabilityFragment extends BaseFragment implements AvailablilityListAdapter.AvailablilityListInterface {

    public static final String TAG = "AvailabilityFragment";

    @BindView(R.id.unavailabilityList)
    RecyclerView unavailabilityList;

    @BindView(R.id.emptyTextView)
    TextView emptyTextView;

    private List<AvailabilityResponse> availabilityResponseList = new ArrayList<>();

    private AvailablilityListAdapter availablilityListAdapter;

    public AvailabilityFragment() {
        // Required empty public constructor
    }

    public static AvailabilityFragment newInstance() {
        AvailabilityFragment fragment = new AvailabilityFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(getString(R.string.unavailability));

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_availibility, container, false);
        unbinder = ButterKnife.bind(this, view);
        mProgressView = view.findViewById(R.id.progressBar);
        errorDialog = new ErrorDialog(getActivity(), getString(R.string.something_went_wrong));
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        availablilityListAdapter = new AvailablilityListAdapter(availabilityResponseList, this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        unavailabilityList.setLayoutManager(mLayoutManager);
        unavailabilityList.addItemDecoration(new DividerItemDecoration(unavailabilityList.getContext()));
        unavailabilityList.setAdapter(availablilityListAdapter);

    }

    @Override
    public void onResume() {
        super.onResume();
        fetchAvailabilityList();

    }

    private void fetchAvailabilityList() {
        showProgressBar(true);
        String id = PreferenceManager.getInstance().getId();
        String user_token = PreferenceManager.getInstance().getToken();
        Call<BaseResponse<List<AvailabilityResponse>>> call = RailApplication.getWebService().getMyAvailability(id, user_token);
        call.enqueue(new Callback<BaseResponse<List<AvailabilityResponse>>>() {
            @Override
            public void onResponse(Call<BaseResponse<List<AvailabilityResponse>>> call, Response<BaseResponse<List<AvailabilityResponse>>> response) {
                showProgressBar(false);
                try {
                    BaseResponse<List<AvailabilityResponse>> availabilityService = response.body();
                    availabilityResponseList = availabilityService.getResponse();
                    availablilityListAdapter.setAvailabilityResponseList(availabilityResponseList);
                    availablilityListAdapter.notifyDataSetChanged();
                    if (availabilityResponseList == null || availabilityResponseList.size() == 0) {
                        emptyTextView.setVisibility(View.VISIBLE);
                    } else {
                        unavailabilityList.smoothScrollToPosition(availabilityResponseList.size());
                    }
                } catch (Exception e) {
                    showProgressBar(false);
                    errorDialog.show();
                    if(emptyTextView != null)
                    emptyTextView.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onFailure(Call<BaseResponse<List<AvailabilityResponse>>> call, Throwable t) {
                showProgressBar(false);
                errorDialog.show();
                emptyTextView.setVisibility(View.VISIBLE);
            }
        });
    }

    @OnClick(R.id.btnAddUnavailability)
    void moveToUnavailibilityActivity() {
        Intent intent = new Intent(getActivity(), AddUnavailabilityActivity.class);
        startActivityForResult(intent, Constants.ADD_AVAILIBILTY_REQUEST_CODE);
    }

    @Override
    public void onRowClick(Integer id) {
        showConfirmDialog(id);
    }
    public void deleteUnavailbility(Integer id){
        showProgressBar(true);
        String userId = PreferenceManager.getInstance().getId();
        String user_token = PreferenceManager.getInstance().getToken();
        Call<ResponseBody> call = RailApplication.getWebService().deleteUnavailability(userId, user_token,id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                showProgressBar(false);
                fetchAvailabilityList();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                showProgressBar(false);
                errorDialog.show();
            }
        });
    }
    public void showConfirmDialog(final Integer id){
        new AlertDialog.Builder(getActivity())
                .setTitle("Delete Unavailability")
                .setMessage("Do you really want to delete?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        deleteUnavailbility(id);
                    }})
                .setNegativeButton(android.R.string.no, null).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        emptyTextView.setVisibility(View.INVISIBLE);

    }
}
