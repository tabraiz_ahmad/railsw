package com.blueshield.appsws.otto;

public class MessageEvent {

    public final String messageId;

    public MessageEvent(String messageId) {
        this.messageId = messageId;
    }

    @Override public String toString() {
        return messageId;
    }
}
