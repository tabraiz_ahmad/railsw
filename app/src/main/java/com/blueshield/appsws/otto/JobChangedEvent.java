package com.blueshield.appsws.otto;

public class JobChangedEvent {

    public final int jobId;
    public final int resourceRequirementId;

    public JobChangedEvent(int jobId,int resourceRequirementId) {
        this.jobId = jobId;
        this.resourceRequirementId = resourceRequirementId;
    }

    @Override public String toString() {
        return new StringBuilder("(") //
                .append(jobId) //
                .append(", ") //
                .append(resourceRequirementId) //
                .append(")") //
                .toString();
    }
}
